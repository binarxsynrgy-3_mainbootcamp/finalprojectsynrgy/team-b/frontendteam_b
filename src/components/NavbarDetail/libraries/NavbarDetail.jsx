import { useRouter } from 'next/router';
import Link from 'next/link';

export default function NavbarDetail() {
  const router = useRouter();
  const { campaignId } = router.query;

  return (
    <div className="flex flex-start gap-8 text-xl font-semibold border-t-2 border-b-2 p-5 border-gray-150">
      <Link href={`/campaign/${campaignId}/detail`}>
        <a className={`${router.pathname.includes('/detail') && 'text-green-550 underline'} text-gray-900 hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Informasi Detail</a>
      </Link>
      <Link href={`/campaign/${campaignId}/news`}>
        <a className={`${router.pathname.includes('/news') && 'text-green-550 underline'} text-gray-900 hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Kabar Terbaru</a>
      </Link>
      <Link href={`/campaign/${campaignId}/faqs`}>
        <a className={`${router.pathname.includes('/faqs') && 'text-green-550 underline'} text-gray-900 hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>FAQ</a>
      </Link>
      <Link href={`/campaign/${campaignId}/messages`}>
        <a className={`${router.pathname.includes('/messages') && 'text-green-550 underline'} text-gray-900 hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Pesan Rekan Asa</a>
      </Link>

    </div>
  );
}
