/* eslint-disable max-len */
// import { useState } from 'react';
import { useRouter } from 'next/router';
// import { useUserDispatch } from '@/redux/reducers/user/slices';
import Link from 'next/link';

export default function NavbarProfile() {
  const router = useRouter();

  return (
    <div className="border-b w-full">
      <div className="flex h-full container mx-auto">
        <div className="flex items-end justify-end space-x-3 mb-3 ml-8">
          <Link href="/profile">
            <a className={`${router.pathname.includes('/profile', '/address', '/password') && 'text-green-250 underline'} text-lg sm:text-sm text-gray-900 font-bold hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Profil Saya</a>
          </Link>
        </div>
        <div className="flex items-end justify-end space-x-3 mb-3 ">
          <Link href="/bookmark">
            <a className={`${router.pathname.includes('/bookmark') && 'text-green-250 underline'} text-lg sm:text-sm text-gray-900 font-bold hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Campaign Tersimpan</a>
          </Link>

        </div>
        <div className="flex items-end justify-end space-x-3 mb-3 ">
          <Link href="/history">
            <a className={`${router.pathname.includes('/history') && 'text-green-250 underline'} text-lg sm:text-sm text-gray-900 font-bold hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Donasi Saya</a>
          </Link>

        </div>
        <div className="flex items-end justify-end space-x-3 mb-3 ">
          <Link href="/mycampaign">
            <a className={`${router.pathname.includes('/mycampaign') && 'text-green-250 underline'} text-lg sm:text-sm text-gray-900 font-bold hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Campaign Saya</a>
          </Link>
        </div>
        {/* <div className="flex items-end justify-end space-x-3 mb-3 ">
          <a href="" className="text-lg sm:text-sm text-gray-900 font-bold hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline">Galeri Karya</a>
        </div> */}
      </div>
    </div>
  );
}
