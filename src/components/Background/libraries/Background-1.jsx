export default function Background1() {
  return (
    <div className="-z-100 inset-0 absolute">
      <img src="/img/background/leaf-vector-right.png" alt="leaf-right" className="float-right md:mt-40 sm:mt-64" />
      <img src="/img/background/leaf-vector-left.png" alt="leaf-left" className="top-9 md:mt-80 sm:mt-96" />
    </div>
  );
}
