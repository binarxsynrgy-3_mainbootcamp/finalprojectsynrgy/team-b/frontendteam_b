/* eslint-disable jsx-a11y/anchor-is-valid */
// import Link from 'next/link';

export default function Footer() {
  return (
    <footer className="text-sm w-full bg-gray-450 text-white">
      <div className="container flex sm:flex-col md:flex-row gap-10 justify-between mx-auto py-10 px-5">
        <div className="flex flex-col">
          <h2 className="text-xl text-center mb-1">Menu</h2>
          <ul className="grid grid-cols-2 content-center gap-x-6  gap-y-1 mx-auto">
            <li>
              <a href="/campaign">Campaign</a>
            </li>
            <li>
              <a href="/login">Login</a>
            </li>
            <li>
              <a href="/about">About Us</a>
            </li>
            <li>
              <a href="/register">Register</a>
            </li>
            <li>
              <a href="/galeri">Galeri Karya</a>
            </li>
          </ul>
        </div>

        <div className="flex flex-col gap-1 text-center">
          <h2 className="text-xl text-center mb-1">Kontak</h2>
          <div>Jakarta Selatan</div>
          <div>asakarya.indonesia@gmail.com</div>
          <div>021-14007</div>
        </div>

        <div className="flex flex-col gap-1">
          <h2 className="text-xl text-center mb-1">Media sosial</h2>
          <div className="flex gap-5 items-center justify-center">
            <a href="#">
              <img src="/img/footer/insta.png" alt="IG" className="cursor-pointer" />
            </a>
            <a href="#">
              <img src="/img/footer/twitter.png" alt="IG" className="cursor-pointer" />
            </a>
            <a href="#">
              <img src="/img/footer/fb.png" alt="IG" className="cursor-pointer" />
            </a>
            <a href="#">
              <img src="/img/footer/yt.png" alt="IG" className="cursor-pointer" />
            </a>
          </div>
        </div>

        <div className="flex flex-col gap-2 items-center">
          <h2 className="text-xl text-center">Download Aplikasi Asakarya</h2>
          <a href="#">
            <img src="/img/footer/google-play-badge.png" alt="App" width="194px" className="cursor-pointer" />
          </a>

        </div>
      </div>
    </footer>
  );
}
