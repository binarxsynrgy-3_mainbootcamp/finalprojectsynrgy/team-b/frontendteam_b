/* eslint-disable react/prop-types */
import { useFormikContext } from 'formik';

export default function Input({ ...props }) {
  const {
    handleChange,
    handleBlur,
    values,
  } = useFormikContext();
  return (
    <div className="w-full text-sm">
      <label htmlFor={props.name} className="block border-gray-300 w-full">
        <input
          {...props}
          className="w-full px-4 border rounded-lg py-2 focus:outline-none focus:border-green-250"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values && values[props.name]}
          type={props.type}
          placeholder={props.placeholder}
        />
      </label>
    </div>
  );
}
