import Image from 'next/image';
import Link from 'next/link';
import { name, profileImage } from '@/helpers/auth';
import { convertDateToString } from '@/helpers/dateTime';

export default function NavbarAdmin() {
  const imgUrl = profileImage();
  const fullName = name();
  const today = new Date();
  const date = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
  return (
    <>
      <nav className="p-8 flex flex-start justify-between w-full">
        <div className="text-3xl font-semibold">
          <Link href="/admin/dashboard">
            <span className="hover:text-gray-650 cursor-pointer">Dashboard</span>
          </Link>
        </div>
        <div className="flex text-xl items-center gap-5">
          <div>{fullName}</div>
          <Image src={imgUrl || '/img/profile/avatar.svg'} width={35} height={35} className="rounded-full" />
        </div>
      </nav>
      <div className="font-semibold text-center">{convertDateToString(date)}</div>
    </>

  );
}
