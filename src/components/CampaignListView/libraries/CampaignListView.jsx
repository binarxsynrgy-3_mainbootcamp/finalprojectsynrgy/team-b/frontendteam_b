/* eslint-disable no-nested-ternary */
import Image from 'next/image';
import Link from 'next/link';
import { ClockIcon } from '@heroicons/react/solid';
import { BookmarkIcon } from '@heroicons/react/outline';
import ProgressBar from '@/components/ProgressBar';
import DonationModal from '@/components/PopupDonation';
import EmptyImage from '@/components/EmptyImage';
import { sessionUser } from '@/helpers/auth';

export default function CampaignListView({
  detailed = true,
  monitoring = false,
  buttonText = 'Dukung Karya Sekarang',
  admin = false,
  ...item
}) {
  return (
    <div className={`flex w-full transition duration-300 rounded-xl ${detailed ? '' : 'hover:shadow-2xl'}`}>

      {item.imgUrl ? <Image src={item.imgUrl} width={admin ? '497' : '694'} height={admin ? '335' : '425'} className="object-contain" /> : <EmptyImage w={admin ? '497' : '694'} h={admin ? '335' : '425'} source="default" />}
      <div className="flex flex-col justify-between flex-grow w-1/3 p-8 gap-5">
        <div className="flex flex-col gap-3">

          {monitoring && (
          <div className="flex justify-between text-xl font-semibold">
            <div>Status Campaign</div>
            <div>
              {
                item.status === 0 && (
                  <div className="flex">
                    <Image src="/img/donation/diproses.svg" width={25} height={25} />
                    <div className="font-semibold ml-3">Diproses</div>
                  </div>
                )
              }
              {
                item.status === 1 && (
                  item.daysLeft < 0
                    ? (
                      <div className="flex">
                        <Image src="/img/donation/overdue.svg" width={25} height={25} />
                        <div className="font-semibold ml-3">Lewat Batas Waktu</div>
                      </div>
                    )
                    : (
                      <div className="flex">
                        <Image src="/img/donation/berhasil.svg" width={25} height={25} />
                        <div className="font-semibold ml-3">Terverifikasi</div>
                      </div>
                    )
                )
               }
              {
                item.status === 2 && (
                  <div className="flex">
                    <Image src="/img/donation/gagal.svg" width={25} height={25} />
                    <div className="font-semibold ml-3">Gagal</div>
                  </div>
                )
               }

            </div>
          </div>
          )}

          <div className="flex justify-between text-md text-green-250">
            <p>{item.categoryName}</p>
            <div className={detailed ? 'hidden' : 'flex text-sm text-gray-350 items-center my-1 gap-1'}>
              <ClockIcon width={14} height={14} />
              <div>
                {item.daysLeft}
                {' '}
                hari tersisa
              </div>
            </div>
          </div>

          <div className={`flex justify-between text-xl ${admin && 'hidden'}`}>
            <h2 className="text-3xl">{item.title}</h2>
            <BookmarkIcon width="19" height="25" className={`${monitoring && 'hidden'}`} />
          </div>

          <ProgressBar detailed={detailed} {...item} />
          <div>
            <span className=" text-green-550 text-xl font-bold">
              Rp.
              {' '}
              {(item.sumDonation).toLocaleString('id-ID')}
              ,-
              {' '}
            </span>
            <span className="text-sm break-words">
              terkumpul dari
              {' '}
              {(item.fundAmount).toLocaleString('id-ID')}
              ,-
            </span>
          </div>
          <div className={(monitoring || admin) && 'hidden'}>
            <p>
              {item.description && item.description.length > 150 ? `${item.description.substr(0, 150)}...` : item.description}
            </p>
          </div>

          <div className={`${admin && 'mt-5'} flex gap-2 text-black`}>
            {item.profileImage ? <Image src={item.profileImage} width={40} height={40} className="flex-grow object-cover bg-gray-50 rounded-md" /> : <EmptyImage h={40} w={40} source="profile" />}
            <div className="flex flex-col">
              <div>
                {item.profileFullName}
              </div>
              <div>
                Lokasi Campaign :
                {' '}
                {item.location}
              </div>
            </div>
          </div>
          <div className={(monitoring || admin) && 'hidden'}>
            {detailed && !sessionUser() ? <DonationModal /> : (detailed && sessionUser() ? (
              <div className="flex justify-center">
                <Link href={`/campaign/${item.id}/donation`}>
                  <button type="button" className="bg-green-250 text-white h-12 w-1/2 rounded-lg mt-7 items-center object-center transition duration-300 hover:bg-green-550">{buttonText}</button>
                </Link>
              </div>
            ) : (
              <div className="flex justify-center">
                <Link href={`/campaign/${item.id}/detail`}>
                  <button type="button" className="bg-green-250 text-white h-12 w-1/2 rounded-lg mt-7 items-center object-center transition duration-300 hover:bg-green-550">{buttonText}</button>
                </Link>
              </div>
            )) }
          </div>
        </div>
        {monitoring && (
          <div className="text-lg underline -mb-8 bottom-0">
            <Link href={`/campaign/${item.id}/detail`}>
              <a className="hover:text-green-250">
                Menuju Detail Campaign
              </a>

            </Link>
          </div>
        )}
      </div>

    </div>
  );
}
