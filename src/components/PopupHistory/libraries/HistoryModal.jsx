import React, { useState } from 'react';
import Image from 'next/image';
import Modal from 'react-modal';
import Link from 'next/link';
import moment from 'moment';
import 'moment/locale/id';

const customStyles = {
  content: {
    width: '630px',
    height: '535px',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

export default function History({ ...donation }) {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  return (
    <>
      <div className="rounded-t">
        <button type="button" className="flex justify-center items-center gap-2" onClick={() => setModalIsOpen(true)}>
          {donation.status === 0 && (
          <>
            <Image src="/img/donation/diproses.jpg" width={25} height={25} />
            {' '}
            <div>Diproses</div>
          </>
          )}
          {donation.status === 1 && (
          <>
            <Image src="/img/donation/berhasil.jpg" width={25} height={25} />
            {' '}
            <div>Berhasil</div>
          </>
          )}
          {donation.status === 2 && (
          <>
            <Image src="/img/donation/gagal.jpg" width={25} height={25} />
            {' '}
            <div>Gagal</div>
          </>
          )}
        </button>
        <Modal
          isOpen={modalIsOpen}
          shouldCloseOnOverlayClick={false}
          onRequestClose={() => setModalIsOpen(false)}
          ariaHideApp={false}
          style={customStyles}
        >
          <div className="flex justify-end items-end">
            <button type="button" onClick={() => setModalIsOpen(false)}>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd" />
              </svg>
            </button>
          </div>

          <div className="mt-8">
            {donation.status === 0 && (
            <>
              <h1 className="text-black text-4xl text-center font-semibold">Terima Kasih!</h1>
              <div className="text-lg text-black mt-4 text-center overflow-auto mx-8">
                Saat ini donasi kamu sedang kami verifikasi ya...
              </div>
            </>
            )}
            {donation.status === 1 && (
            <>
              <h1 className="text-black text-4xl text-center font-semibold">Terima Kasih!</h1>
              <div className="text-lg text-black mt-4 text-center overflow-auto mx-8">
                Donasi kamu telah kami terima dan akan kami salurkan
              </div>
            </>
            )}
            {donation.status === 2 && (
            <>
              <h1 className="text-black text-4xl text-center font-semibold">Mohon Maaf...</h1>
              <div className="text-lg text-black mt-4 text-center overflow-auto mx-8">
                Donasi gagal terverifikasi, harap hubungi kami jika
                kamu merasa sudah transfer dengan benar
              </div>
            </>
            )}
            <div className="flex justify-center items-center mt-6">
              <div className="border rounded-lg border-gray-300 bg-white w-5/6 h-8/12 px-6 py-6">
                <div className="grid-cols-2 flex justify-start items-center gap-4">
                  <div className="col-span-1">
                    <div>Tanggal</div>
                    <div>Metode Pembayaran</div>
                    <div>ID Donasi</div>
                    <div>Status</div>
                  </div>
                  <div className="col-span-1 font-semibold">
                    <div>{moment(donation.createdAt).format('LLL')}</div>
                    <div>Transfer Bank</div>
                    <div>{donation.id}</div>
                    <div className="flex gap-2">
                      {donation.status === 0 && (
                      <>
                        <Image src="/img/donation/diproses.jpg" width={25} height={25} />
                        {' '}
                        <div>Diproses</div>
                      </>
                      )}
                      {donation.status === 1 && (
                      <>
                        <Image src="/img/donation/berhasil.jpg" width={25} height={25} />
                        {' '}
                        <div>Berhasil</div>
                      </>
                      )}
                      {donation.status === 2 && (
                      <>
                        <Image src="/img/donation/gagal.jpg" width={25} height={25} />
                        {' '}
                        <div>Gagal</div>
                      </>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex items-center justify-center mt-8">
              <Link href={`/campaign/${donation.campaignId}/detail`}>
                <button type="button" className="bg-green-250 text-white w-56 h-12 text-xl rounded-xl font-normal">Donasi Lagi</button>
              </Link>
            </div>
            <div className="text-sm text-black font-normal text-center mt-8">
              <div>021-14007</div>
              <div>asakarya.indonesia@gmail.com</div>
            </div>
          </div>

        </Modal>
      </div>
    </>
  );
}
