import React, { useState } from 'react';
import Image from 'next/image';
import Modal from 'react-modal';

const customStyles = {
  content: {
    width: '630px',
    height: '535px',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

export default function DeleteAddress({ ...props }) {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const handleOpenModal = () => {
    setModalIsOpen(true);
  };
  return (
    <>
      <div className="rounded-t">
        <button type="button" className="col-span-1 text-md underline focus:underline focus:text-green-250 cursor-pointer" onClick={() => handleOpenModal()}>Hapus</button>
        <Modal
          isOpen={modalIsOpen}
          shouldCloseOnOverlayClick={false}
          onRequestClose={() => setModalIsOpen(false)}
          ariaHideApp={false}
          style={customStyles}
        >
          <div className="flex justify-end items-end">
            <button type="button" onClick={() => setModalIsOpen(false)}>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd" />
              </svg>
            </button>
          </div>
          <div className="flex justify-center items-center">
            <Image
              src="/img/logo/logo-with-text.png"
              width={80}
              height={110}
            />
          </div>
          <div>
            {props.modifyNews ? (<h1 className="text-black text-4xl text-center font-normal mt-4">Yakin Hapus Kabar?</h1>) : (<h1 className="text-black text-4xl text-center font-normal mt-4">Yakin Hapus Alamat?</h1>)}
            <div className="flex justify-center items-center mt-16">
              <div className="pt-2 flex justify-center items-center gap-4">
                <button
                  className="bg-white text-lg text-green-250 border-green-250 border rounded-lg font-bold py-3 w-40 h-15 hover:bg-green-550 hover:text-white transition duration-300"
                  type="button"
                  onClick={props.onConfirm}
                >
                  Hapus
                </button>
                <button
                  className="bg-green-250 text-lg text-white border-white border rounded-lg font-bold py-3 w-40 h-15 hover:bg-green-550 transition duration-300"
                  type="button"
                  onClick={() => setModalIsOpen(false)}
                >
                  Batalkan
                </button>
              </div>
            </div>

            <div className="text-lg text-black font-normal text-center mt-28">
              <div>021-14007</div>
              <div>asakarya.indonesia@gmail.com</div>
            </div>
          </div>
        </Modal>
      </div>
    </>
  );
}
