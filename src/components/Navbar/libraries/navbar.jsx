/* eslint-disable max-len */
/* eslint-disable no-console */
import Image from 'next/image';
// import { useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import EmptyImage from '@/components/EmptyImage';
import { sessionUser, profileImage } from '@/helpers/auth';

export default function Navbar() {
  const token = sessionUser();
  const imgUrl = profileImage();
  // console.log(imgUrl);

  const router = useRouter();
  // useEffect(() => {
  //   console.log('current =', router.pathname.includes('/profile'));
  // }, [router.pathname]);

  return (
    <nav className="h-16 flex justify-start items-center border-b px-12 mb-4 sticky top-0 bg-white" style={{ zIndex: '20' }}>
      <div className="h-full container mx-auto flex justify-between items-center">
        <div className="flex items-center my-4 cursor-pointer">
          <Image width="183" height="54" src="/img/navbar/logo.png" alt="logo" onClick={() => router.push('/')} />
        </div>
        <div className="flex my-5 items-end justify-end space-x-3">
          <div>
            <a href="/about" className={`${router.pathname.includes('/about') && 'text-green-550 underline'} text-lg sm:text-sm text-gray-900 font-bold hover:text-green-250 md:mx-4 md:my-0 focus:text-green-250 hover:underline focus:underline`}>Asakarya</a>
          </div>
          <div>
            <a href="/campaign" className={`${router.pathname.includes('/campaign') && 'text-green-550 underline'} text-lg sm:text-sm text-gray-900 font-bold hover:text-green-250 md:mx-4 md:my-0 focus:text-green-250 hover:underline focus:underline`}>Campaign</a>
          </div>
          <div>
            <a href="/galeri" className={`${router.pathname.includes('/galeri') && 'text-green-550 underline'} text-lg sm:text-sm text-gray-900 font-bold hover:text-green-250 md:mx-4 md:my-0 focus:text-green-250 hover:underline focus:underline`}>Galeri Karya</a>
          </div>
          {token ? (
            <div className="flex space-x-6 justify-between">
              {imgUrl ? (
                <Image className={`${router.pathname.includes('/profile') && 'border-green-550'} object-cover w-8 h-8 rounded-full cursor-pointer`} src={imgUrl} alt="Profile image" width="40" height="40" onClick={() => router.push('/profile')} />
              ) : (
                <Link href="/profile">
                  <div className="cursor-pointer flex items-center justify-center">
                    <EmptyImage w="40" h="40" source="profile" />
                  </div>
                </Link>
              )}
            </div>
          ) : (
            <a href="/login" className="text-lg text-gray-900 font-bold hover:text-green-250 md:mx-4 md:my-0 focus:text-green-250 hover:underline">Masuk</a>
          )}
        </div>
      </div>
    </nav>
  );
}
