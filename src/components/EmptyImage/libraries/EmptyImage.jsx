/* eslint-disable no-nested-ternary */
import Image from 'next/image';

export default function EmptyImage({ w, h, source }) {
  return (
    <Image
      className="object-contain"
      width={w}
      height={h}
      src={
        (source === 'campaign' ? '/img/empty/boxes.svg'
          : (source === 'profile' ? '/img/empty/avatar.svg'
            : '/img/empty/default.svg'))
      }
    />
  );
}
