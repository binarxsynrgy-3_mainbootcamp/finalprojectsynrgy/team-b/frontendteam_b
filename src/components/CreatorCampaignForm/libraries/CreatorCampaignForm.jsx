/* eslint-disable no-console */
import Image from 'next/image';
import { Formik, Form } from 'formik';
import { useState } from 'react';
import GiftModal from '@/components/PopupGift';
import FaqNews from '@/components/PopupFAQ';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import { baseApiUrl } from '@/helpers/baseContents';
import { sessionUser } from '@/helpers/auth';

export default function CreatorCampaignForm({
  campaignId,
  onSubmit,
  loading,
  categories,
  ...campaignDetail
}) {
  const [preview, setPreview] = useState();
  const { campaign } = useCampaignDispatch();
  const { rewards } = campaign;

  const initialValuesEmpty = {
    title: '',
    description: '',
    fundAmount: null,
    due: '',
    location: '',
    categoryId: null,
    imgUrl: null,
  };

  const initialValues = {
    id: campaignDetail.id,
    title: campaignDetail.title,
    description: campaignDetail.description,
    fundAmount: campaignDetail.fundAmount,
    due: campaignDetail.due,
    location: campaignDetail.location,
    categoryId: campaignDetail.categoryId,
    imgUrl: campaignDetail.imgUrl,
  };

  const handleResetRewards = async (item) => {
    const id = item[0].campaignId;
    const token = sessionUser();
    try {
      const response = await fetch(`${baseApiUrl}/reward/delete/campaign/${id}`, {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${token}`,
        },

      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
    window.location.reload();
  };

  const renderCategories = () => categories && categories.map((item, idx) => (
    (idx > 0 ? (<option key={`selectCategory-${item.id}`} value={`${item.id}`}>{item.categoryName}</option>) : null)
  ));

  return (
    <Formik
      initialValues={campaignDetail && campaignDetail.id ? initialValues : initialValuesEmpty}
      onSubmit={onSubmit}
      enableReinitialize
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        setFieldValue,
      }) => (
        <Form onSubmit={handleSubmit} className="grid grid-cols-4">

          <div className="grid grid-cols-4 gap-3 p-3 mx-3 col-span-3">
            <div className="ml-3 my-auto">Judul Campaign</div>
            <input
              id="title"
              name="title"
              type="text"
              placeholder="Masukkan judul campaign"
              className="w-full px-4 border rounded-lg py-2 focus:border-green-250 col-span-3"
              onChange={handleChange}
              onBlur={handleBlur}
              defaultValue={campaignDetail.title}
              required
            />

            <div className="ml-3 my-auto">Target Donasi</div>
            <input
              id="fundAmount"
              name="fundAmount"
              type="number"
              min="0"
              placeholder="Rp. 5.000.000"
              className="w-full px-4 border rounded-lg py-2 focus:border-green-250 col-span-3"
              onChange={handleChange}
              onBlur={handleBlur}
              defaultValue={campaignDetail.fundAmount}
              required
            />

            <div className="ml-3 my-auto">Campaign Berakhir</div>
            <input
              id="due"
              name="due"
              type="date"
              className="w-full px-4 border rounded-lg py-2 focus:border-green-250 col-span-3"
              onChange={handleChange}
              onBlur={handleBlur}
              defaultValue={campaignDetail.due}
              required
            />

            <div className="ml-3 my-auto">Lokasi Campaign</div>
            <input
              id="location"
              name="location"
              type="text"
              placeholder="Kota, Negara"
              className="w-full px-4 border rounded-lg py-2 focus:border-green-250 col-span-3"
              onChange={handleChange}
              onBlur={handleBlur}
              defaultValue={campaignDetail.location}
              required
            />

            <div className="ml-3 my-auto">Kategori Campaign</div>
            <select
              name="categoryId"
              id="categoryId"
              className="w-full px-4 border rounded-lg py-2 focus:border-green-250 col-span-3"
              onChange={handleChange}
              onBlur={handleBlur}
              defaultValue={campaignDetail.categoryId ? campaignDetail.categoryId : ''}
              required
            >
              <option value="" disabled className="text-gray-150">{categories ? 'Pilih kategori' : 'loading' }</option>
              {renderCategories()}
            </select>

            <div className="ml-3 my-2">Deskripsi Campaign</div>
            <textarea
              name="description"
              id="description"
              cols="30"
              rows="20"
              placeholder="Masukkan deskripsi campaign"
              className="px-4 py-2 border border-gray-150 rounded-lg col-span-3"
              onChange={handleChange}
              onBlur={handleBlur}
              defaultValue={campaignDetail.description}
              required
            />
          </div>
          <div className="px-3 my-3 border-l-2 border-gray-150 flex flex-col items-center gap-5">
            {
                    preview ? (
                      <div>
                        <Image src={preview} className="rounded-md" alt="preview-foto" height="132" width="187" />
                      </div>
                    )
                      : (
                        <Image src={campaignDetail && campaignDetail.imgUrl ? campaignDetail.imgUrl : '/img/create/pic.png'} width="187" height="132" />
                      )
                  }

            <div className="text-center">
              <label htmlFor="imgFile" className="border flex items-center px-4 rounded-lg h-8 hover:bg-green-250 hover:text-white transition duration-300 cursor-pointer mb-2">
                <div className="mx-auto">Pilih Gambar</div>
                <input
                  type="file"
                  id="imgFile"
                  name="imgFile"
                  className="hidden"
                  onChange={(event) => {
                    if (event.target.files.length !== 0) {
                      setFieldValue('imgFile', event.currentTarget.files[0]);
                      const { files } = event.target;
                      const tempURL = URL.createObjectURL(files[0]);
                      setPreview(tempURL);
                    }
                  }}
                  accept=".PNG, .JPEG, .JPG"
                />
              </label>

              <input
                id="imgUrl"
                name="imgUrl"
                type="text"
                placeholder="Kota, Negara"
                className="w-full px-4 border rounded-lg py-2 focus:border-green-250 col-span-3 hidden"
                onChange={handleChange}
                onBlur={handleBlur}
                defaultValue={campaignDetail.imgUrl}
              />

              <div className="text-xs text-gray-325">Ukuran gambar: maks. 2MB</div>
              <div className="text-xs text-gray-325">Format gambar: .JPEG, .JPG, .PNG</div>
            </div>

            <Image src="/img/create/reward.png" width="187" height="132" />
            <GiftModal />
            {(rewards && rewards.length > 0) && (
            <FaqNews source="Hadiah" onConfirm={() => handleResetRewards(rewards)} />
            )}

          </div>

          <div className="flex justify-center col-span-4">
            <button type="submit" className={`${loading ? 'bg-gray-150' : 'bg-green-250 hover:bg-green-550'} text-white font-semibold h-10 w-1/6 mt-3 rounded-lg transition duration-300`} disabled={!!loading}>{loading ? 'Mohon Tunggu' : 'Simpan'}</button>
          </div>

        </Form>
      )}

    </Formik>
  );
}
