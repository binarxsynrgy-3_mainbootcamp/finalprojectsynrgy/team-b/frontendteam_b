import Image from 'next/image';
import { Formik, Form } from 'formik';
import { useState } from 'react';

export default function CreatorNewsForm({
  campaignId, onSubmit, loading, ...newsDetail
}) {
  const initialValuesEmpty = {
    title: '',
    activity: '',
    imgUrl: '',
    campaignId,
  };

  const initialValues = {
    title: newsDetail.title,
    activity: newsDetail.activity,
    imgUrl: newsDetail.imgUrl,
    id: newsDetail.id,
    campaignId,
  };

  const [preview, setPreview] = useState();

  return (
    <Formik
      initialValues={newsDetail.title ? initialValues : initialValuesEmpty}
      onSubmit={onSubmit}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        setFieldValue,
      }) => (
        <Form onSubmit={handleSubmit} className="grid grid-cols-4">

          <div className="grid grid-cols-4 gap-3 p-3 mx-3 col-span-3">
            <div className="ml-3 my-auto">Judul Kabar Terbaru</div>
            <input
              id="title"
              name="title"
              type="text"
              placeholder="Masukkan judul kabar terbaru"
              className="w-full px-4 border rounded-lg py-2 focus:border-green-250 col-span-3"
              onChange={handleChange}
              onBlur={handleBlur}
              defaultValue={newsDetail.title}
              required
            />

            <div className="ml-3 my-2">Deskripsi Kabar</div>
            <textarea
              name="activity"
              id="activity"
              cols="40"
              rows="30"
              placeholder="Masukkan deskripsi kabar tebaru"
              className="px-4 py-2 border border-gray-150 rounded-lg col-span-3"
              onChange={handleChange}
              onBlur={handleBlur}
              defaultValue={newsDetail.activity}
              required
            />
          </div>
          <div className="px-3 my-3 border-l-2 border-gray-150 flex flex-col items-center gap-5">
            {
              preview ? (
                <div>
                  <Image src={preview} className="rounded-md object-contain" alt="preview-foto" width="187" height="132" />
                </div>
              )
                : (
                  <Image src={newsDetail && newsDetail.imgUrl ? newsDetail.imgUrl : '/img/create/pic.png'} width="187" height="132" />
                )
            }

            <div className="text-center">
              <label htmlFor="imgUrl" className="border flex items-center px-4 rounded-lg h-8 hover:bg-green-250 hover:text-white transition duration-300 cursor-pointer mb-2">
                <div className="mx-auto">Pilih Gambar</div>
                <input
                  type="file"
                  id="imgUrl"
                  name="imgUrl"
                  className="hidden"
                  onChange={(event) => {
                    if (event.target.files.length !== 0) {
                      setFieldValue('imgUrl', event.currentTarget.files[0]);
                      const { files } = event.target;
                      const tempURL = URL.createObjectURL(files[0]);
                      setPreview(tempURL);
                    }
                  }}
                  accept=".PNG, .JPG, .JPEG"
                />
              </label>

              <div className="text-xs text-gray-325">Ukuran gambar: maks. 2MB</div>
              <div className="text-xs text-gray-325">Format gambar: .JPEG, .PNG</div>
            </div>
          </div>
          <div className="flex justify-center col-span-4">
            <button type="submit" className={`${loading ? 'bg-gray-150' : 'bg-green-250'} text-white font-semibold h-10 w-1/6 mt-3 rounded-lg transition duration-300 hover:bg-green-550`} disabled={!!loading}>{loading ? 'Mohon Tunggu' : 'Simpan'}</button>
          </div>

        </Form>
      )}

    </Formik>
  );
}
