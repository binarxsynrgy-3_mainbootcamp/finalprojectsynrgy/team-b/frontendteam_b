/* eslint-disable no-plusplus */
import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import Modal from 'react-modal';
import { useFormikContext } from 'formik';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import UploadPicture from '@/components/UploadPict';

const customStyles = {
  content: {
    width: '730px',
    height: '650px',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

export default function GiftModal() {
  const { setFieldValue, handleBlur, handleChange } = useFormikContext();
  const { campaign } = useCampaignDispatch();
  const { rewards } = campaign;
  const [tempFill, setTempFill] = useState();
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const formObject = {};

  const handleSaveForm = () => {
    const item1 = document.getElementById('item1').value;
    const item2 = document.getElementById('item2').value;
    const item3 = document.getElementById('item3').value;
    formObject.item1 = item1;
    formObject.item2 = item2;
    formObject.item3 = item3;
    setTempFill(formObject);
    // console.log(formObject);
    setModalIsOpen(false);
  };

  useEffect(() => {
    // console.log(tempFill);
    for (let i = 0; i < rewards.length; i++) {
      if (rewards[i].rewardTypeId === 1) { formObject.item1 = rewards[i].item; }
      if (rewards[i].rewardTypeId === 2) { formObject.item2 = rewards[i].item; }
      if (rewards[i].rewardTypeId === 2) { formObject.item3 = rewards[i].item; }
    }
    setTempFill(formObject);
  }, [rewards]);

  return (
    <>
      <div className="rounded-t">
        <button type="button" className="border px-4 rounded-lg h-8 hover:bg-green-250 hover:text-white transition duration-300 w-full mb-2" onClick={() => setModalIsOpen(true)}>Hadiah Donasi</button>
        <Modal
          isOpen={modalIsOpen}
          shouldCloseOnOverlayClick={false}
          onRequestClose={() => setModalIsOpen(false)}
          ariaHideApp={false}
          style={customStyles}
        >
          <div className="flex justify-end items-end">
            <button type="button" onClick={() => setModalIsOpen(false)}>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd" />
              </svg>
            </button>
          </div>
          <div className="flex justify-center items-center">
            <Image
              src="/img/logo/logo-with-text.png"
              width={70}
              height={100}
            />
          </div>
          <div>
            <h1 className="text-black text-4xl text-center font-semibold mt-2">Hadiah Donasi</h1>
            <div className="text-lg text-black mt-2 text-center overflow-auto">
              Atur hadiah donasi campaign kamu agar campaign kamu menarik
              sekaligus bentuk terima kasih untuk pendukung karyamu!
            </div>
            <div className="flex justify-center items-center mt-3">
              <div className="border p-5 rounded-lg border-gray-300 bg-white w-5/6 h-8/12">

                <div className="flex gap-1">
                  <div className="text-sm text-black font-bold">Hadiah Paket 1 </div>
                  <div className="text-sm text-gray-350 font-normal">(Donasi Rp 100.000 - Rp 249.999)</div>
                </div>
                <div className="grid grid-cols-8 justify-center items-center mt-2">
                  <input
                    className="border w-11/12 h-9 rounded-md mb-2 text-xs px-3 col-span-6"
                    title="item1"
                    id="item1"
                    name="item1"
                    placeholder="Masukan deskripsi hadiah donasi Paket 1"
                    type="text"
                    defaultValue={tempFill && tempFill.item1 ? tempFill.item1 : null}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <div className="col-span-2">
                    <UploadPicture
                      id="img1"
                      name="img1"
                      onChange={(event) => {
                        setFieldValue('file', event.currentTarget.files[0]);
                      }}
                    />
                  </div>

                </div>
                <div className="flex gap-1">
                  <div className="text-sm text-black font-bold">Hadiah Paket 2</div>
                  <div className="text-sm text-gray-350 font-normal">(Donasi Rp 250.000 - Rp 499.999)</div>
                </div>
                <div className="grid grid-cols-8 justify-center items-center mt-2">
                  <input
                    className="border w-11/12 h-9 rounded-md mb-2 text-xs px-3 col-span-6"
                    title="item2"
                    id="item2"
                    name="item2"
                    placeholder="Masukan deskripsi hadiah donasi Paket 2"
                    type="text"
                    defaultValue={tempFill && tempFill.item2 ? tempFill.item2 : null}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <div className="col-span-2">
                    <UploadPicture
                      id="img2"
                      name="img2"
                      onChange={(event) => {
                        setFieldValue('file', event.currentTarget.files[0]);
                      }}
                    />
                  </div>

                </div>
                <div className="flex gap-1">
                  <div className="text-sm text-black font-bold">Hadiah Paket 3</div>
                  <div className="text-sm text-gray-350 font-normal">(Donasi di atas Rp 500.000)</div>
                </div>
                <div className="grid grid-cols-8 justify-center items-center mt-2">
                  <input
                    className="border w-11/12 h-9 rounded-md mb-2 text-xs px-3 col-span-6"
                    title="item3"
                    id="item3"
                    name="item3"
                    placeholder="Masukan deskripsi hadiah donasi Paket 3"
                    type="text"
                    defaultValue={tempFill && tempFill.item3 ? tempFill.item3 : null}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <div className="col-span-2">
                    <UploadPicture
                      id="img3"
                      name="img3"
                      onChange={(event) => {
                        setFieldValue('file', event.currentTarget.files[0]);
                      }}
                    />
                  </div>

                </div>
                <div className="pt-2 flex justify-center items-center gap-4">
                  <button
                    className="bg-white text-sm text-green-250 border-2 border-green-250 rounded-lg font-bold py-3 w-44 h-auto"
                    type="button"
                    onClick={() => setModalIsOpen(false)}
                  >
                    Nanti Saja

                  </button>
                  <button className="bg-green-250 text-sm text-white border-none rounded-lg font-bold py-3 w-44 h-auto transition duration-300 hover:bg-green-550" type="button" onClick={() => handleSaveForm()}>
                    Simpan Perubahan
                  </button>
                </div>

              </div>
            </div>

            <div className="text-sm text-black font-normal text-center mt-4">
              <div>021-14007</div>
              <div>asakarya.indonesia@gmail.com</div>
            </div>
          </div>
        </Modal>
        <div className="text-xs text-gray-325">Ukuran gambar: maks. 2MB</div>
        <div className="text-xs text-gray-325">Format gambar: .JPEG, .JPG, .PNG</div>
      </div>

    </>
  );
}
