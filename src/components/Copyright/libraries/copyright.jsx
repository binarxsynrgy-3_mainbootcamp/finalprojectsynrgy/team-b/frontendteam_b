export default function Copyright() {
  return (
    <div className="bg-black h-10 p-2">
      <p className="flex items-center justify-center text-white text-lg">
        AsaKarya, b(erbalik) © 2021
      </p>
    </div>
  );
}
