import moment from 'moment';
import 'moment/locale/id';

export const convertDateToString = (dateString) => moment(dateString).format('LL');
