import { createSlice } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import { baseApiUrl } from '@/helpers/baseContents';

const initialState = {
  dashboard: {},
  campaigns: [],
  donations: [],
  disbursements: [],
  loading: false,
  totalPages: 0,
  page: 0,
  totalDisbursements: 0,
  creatorDetail: {},
};

const slices = createSlice({
  initialState,
  name: 'Admin',
  reducers: {

    setDashboard(state, action) {
      Object.assign(state, {
        ...state,
        dashboard: action.payload,
      });
    },

    setTotalPages(state, action) {
      Object.assign(state, {
        ...state,
        totalPages: action.payload,
      });
    },

    setLoading(state, action) {
      Object.assign(state, {
        ...state,
        loading: action.payload,
      });
    },

    setPage(state, action) {
      Object.assign(state, {
        ...state,
        page: action.payload,
      });
    },

    setCampaigns(state, action) {
      Object.assign(state, {
        ...state,
        campaigns: action.payload,
      });
    },

    setDonations(state, action) {
      Object.assign(state, {
        ...state,
        donations: action.payload,
      });
    },

    setDisbursements(state, action) {
      Object.assign(state, {
        ...state,
        disbursements: action.payload,
      });
    },

    setTotalDisbursements(state, action) {
      Object.assign(state, {
        ...state,
        totalDisbursements: action.payload,
      });
    },

    setCreatorDetail(state, action) {
      Object.assign(state, {
        ...state,
        creatorDetail: action.payload,
      });
    },
  },
});

export const {
  setDashboard,
  setTotalPages,
  setLoading,
  setPage,
  setHistory,
  setCampaigns,
  setDonations,
  setDisbursements,
  setTotalDisbursements,
  setCreatorDetail,
} = slices.actions;

export const useAdminDispatch = () => {
  const ApiUrl = baseApiUrl;
  const { admin } = useSelector((state) => state);
  const dispatch = useDispatch();

  const fetchDashboard = async (params) => {
    const {
      token,
    } = params;
    dispatch(setLoading(true));
    try {
      const endpoint = `${ApiUrl}/admin/dashboard`;
      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      const dashboardData = await data.data;
      dispatch(setDashboard(dashboardData));
    } catch (error) {
      dispatch(setDashboard({}));
    }
    dispatch(setLoading(false));
  };

  const fetchCampaigns = async (params) => {
    const { page = 0, size = 6, token } = params;
    const endpoint = `${ApiUrl}/admin/campaign/all?page=${page}&size=${size}&sortBy=status&sortType=asc`;

    dispatch(setLoading(true));
    dispatch(setPage(page));

    try {
      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      const { totalElements } = await data.data.pageable;
      const loadedCampaigns = await data.data.content;
      const totalPages = Math.ceil(totalElements / size);
      dispatch(setCampaigns(loadedCampaigns));
      dispatch(setTotalPages(totalPages));
    } catch (error) {
      dispatch(setCampaigns([]));
    }
    dispatch(setLoading(false));
  };

  const fetchDonations = async (params) => {
    const {
      page = 0, size = 10, token, campaignId,
    } = params;
    const endpoint = `${ApiUrl}/admin/donation/all?campaignId=${campaignId}&page=${page}&size=${size}&sortBy=status&sortType=asc`;

    dispatch(setLoading(true));
    dispatch(setPage(page));

    try {
      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      const { totalElements } = await data.data.pageable;
      const loadedDonations = await data.data.content;
      const totalPages = Math.ceil(totalElements / size);
      dispatch(setDonations(loadedDonations));
      dispatch(setTotalPages(totalPages));
    } catch (error) {
      dispatch(setDonations([]));
    }
    dispatch(setLoading(false));
  };

  const fetchDisbursements = async (params) => {
    const {
      page = 0, size = 10, token, campaignId,
    } = params;
    const endpoint = `${ApiUrl}/disbursement/all?campaignId=${campaignId}&page=${page}&size=${size}&sortBy=updatedAt&sortType=desc`;

    dispatch(setLoading(true));
    dispatch(setPage(page));

    try {
      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      const { totalElements } = await data.data.pageable;
      const loadedDisbursements = await data.data.content;
      const totalPages = Math.ceil(totalElements / size);
      dispatch(setTotalDisbursements(totalElements));
      dispatch(setDisbursements(loadedDisbursements));
      dispatch(setTotalPages(totalPages));
    } catch (error) {
      dispatch(setDisbursements([]));
    }
    dispatch(setLoading(false));
  };

  const fetchCreatorDetail = async (id, token) => {
    try {
      const response = await fetch(`${baseApiUrl}/creator/detail/${id}`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      dispatch(setCreatorDetail(data.data));
      console.log(data);
    } catch (error) {
      console.log(error);
      dispatch(setCreatorDetail({}));
    }
  };

  return {
    admin,
    fetchDashboard,
    fetchCampaigns,
    fetchDonations,
    fetchDisbursements,
    fetchCreatorDetail,
  };
};
export default slices.reducer;
