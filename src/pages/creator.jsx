import Head from 'next/head';
import FormGalangDanaContainer from '@/containers/formGalangDana';
import MainLayout from '@/layouts/main';
import AuthLayout from '@/layouts/auth';
import NavbarProfile from '@/components/NavbarProfile';

export default function GalangDanaPage() {
  return (
    <>
      <Head>
        <title>Profil Kreator</title>
      </Head>
      <MainLayout>
        <NavbarProfile />
        <AuthLayout>
          <FormGalangDanaContainer />
        </AuthLayout>
      </MainLayout>
    </>
  );
}
