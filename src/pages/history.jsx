import Head from 'next/head';
import HistoryCampaignContainer from '@/containers/historyCampaign';
import MainLayout from '@/layouts/main';
import AuthLayout from '@/layouts/auth';
import NavbarProfile from '@/components/NavbarProfile';

export default function HistoryCampaignPage() {
  return (
    <>
      <Head>
        <title>Donasi Saya</title>
      </Head>
      <MainLayout>
        <NavbarProfile />
        {/* <HistoryCampaignContainer /> */}
        <AuthLayout>
          <HistoryCampaignContainer />
        </AuthLayout>
      </MainLayout>
    </>
  );
}
