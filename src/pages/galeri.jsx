import Head from 'next/head';
import GalleryContainer from '@/containers/gallery';
import MainLayout from '@/layouts/main';

export default function GalleryPage() {
  return (
    <>
      <Head>
        <title>Galeri</title>
      </Head>
      <MainLayout>
        <GalleryContainer />
      </MainLayout>
    </>
  );
}

// export default withAuth(GalleryPage);
