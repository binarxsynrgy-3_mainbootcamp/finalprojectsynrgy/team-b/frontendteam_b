import Head from 'next/head';
import LoginContainer from '@/containers/login';
import Background1 from '@/components/Background/libraries/Background-1';
import MainLayout from '@/layouts/main';
// import AuthLayout from '@/layouts/auth';

export default function LoginPage() {
  return (
    <MainLayout>
      <Head>
        <title>Masuk</title>
      </Head>
      <LoginContainer />
      <Background1 />
    </MainLayout>
  );
}
