import Head from 'next/head';
import AdminCampaignLayout from '@/layouts/adminCampaign';
import AdminCampaignInfoContainer from '@/containers/admin/campaign/info';
import AdminAuthLayout from '@/layouts/authAdmin';

export default function AdminCampaignInfoPage() {
  return (
    <>
      <AdminAuthLayout>
        <Head>
          <title>Informasi Detail</title>
        </Head>
        <AdminCampaignLayout>
          <AdminCampaignInfoContainer />
        </AdminCampaignLayout>
      </AdminAuthLayout>
    </>

  );
}
