import Head from 'next/head';
import AdminCampaignDetailContainer from '@/containers/admin/campaign/detail';
import AdminLayout from '@/layouts/admin';
import AdminAuthLayout from '@/layouts/authAdmin';

export default function AdminCampaignDetailPage() {
  return (
    <>
      <AdminAuthLayout>
        <Head>
          <title>Campaign Detail</title>
        </Head>
        <AdminLayout>
          <AdminCampaignDetailContainer />
        </AdminLayout>
      </AdminAuthLayout>

    </>
  );
}
