import Head from 'next/head';
import AdminCampaignLayout from '@/layouts/adminCampaign';
import AdminDisbursementAddContainer from '@/containers/admin/campaign/disbursementAdd';
import AdminAuthLayout from '@/layouts/authAdmin';

export default function AdminDisbursementAddPage() {
  return (
    <>
      <AdminAuthLayout>
        <Head>
          <title>Informasi Dana Terkirim</title>
        </Head>
        <AdminCampaignLayout>
          <AdminDisbursementAddContainer />
        </AdminCampaignLayout>
      </AdminAuthLayout>
    </>

  );
}
