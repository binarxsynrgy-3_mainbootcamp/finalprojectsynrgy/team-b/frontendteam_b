import Head from 'next/head';
import PaymentContainer from '@/containers/payment/library/PaymentContainer';
import MainLayout from '@/layouts/main';

export default function CampaignPage() {
  return (
    <>
      <Head>
        <title>Checkout Payment</title>
      </Head>
      <MainLayout>
        <PaymentContainer />
      </MainLayout>
    </>
  );
}
