import Head from 'next/head';
import CampaignDetailLayout from '@/layouts/campaignDetail';
import CampaignTestimonialShowContainer from '@/containers/campaign/TestimonialShow';

export default function detailPage() {
  return (
    <>
      <Head>
        <title>Pesan</title>
      </Head>
      <CampaignDetailLayout>
        <CampaignTestimonialShowContainer />
      </CampaignDetailLayout>
    </>
  );
}
