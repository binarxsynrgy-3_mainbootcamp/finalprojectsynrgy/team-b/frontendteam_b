import Head from 'next/head';
import CampaignDetailLayout from '@/layouts/campaignDetail';
import CampaignNewsShowContainer from '@/containers/campaign/NewsShow';

export default function detailPage() {
  return (
    <>
      <Head>
        <title>Kabar Terbaru</title>
      </Head>
      <CampaignDetailLayout>
        <CampaignNewsShowContainer />
      </CampaignDetailLayout>
    </>
  );
}
