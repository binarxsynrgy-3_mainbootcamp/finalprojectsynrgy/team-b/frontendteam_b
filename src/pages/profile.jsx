import Head from 'next/head';
import ProfileContainer from '@/containers/account';
import MainLayout from '@/layouts/main';
import AccountLayout from '@/layouts/account';
import AuthLayout from '@/layouts/auth';

export default function CampaignPage() {
  return (
    <>
      <Head>
        <title>Profil</title>
      </Head>
      <MainLayout>
        <AccountLayout parent="profil">
          <AuthLayout>
            <ProfileContainer />
          </AuthLayout>
        </AccountLayout>
      </MainLayout>
    </>
  );
}
