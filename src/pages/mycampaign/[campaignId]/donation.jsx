import Head from 'next/head';
import CampaignMonitorDonationContainer from '@/containers/campaign/MonitorDonation';
import MonitorLayout from '@/layouts/monitor';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function DonationMonitorPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Monitor Donasi</title>
        </Head>

        <MonitorLayout>
          <CampaignMonitorDonationContainer />
        </MonitorLayout>
      </CreatorAuthLayout>

    </>
  );
}
