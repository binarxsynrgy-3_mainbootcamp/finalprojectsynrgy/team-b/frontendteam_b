import Head from 'next/head';
import MainLayout from '@/layouts/main';
import AccountLayout from '@/layouts/account';
import CreateCampaignNewsContainer from '@/containers/campaign/NewsCreate';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function CreateCampaignNewsPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Tambah Kabar Terbaru</title>
        </Head>
        <MainLayout>
          <AccountLayout parent="createCampaign">
            <CreateCampaignNewsContainer />
          </AccountLayout>
        </MainLayout>
      </CreatorAuthLayout>

    </>
  );
}
