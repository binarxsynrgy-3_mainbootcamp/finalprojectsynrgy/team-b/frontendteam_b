import Head from 'next/head';
import MainLayout from '@/layouts/main';
import AccountLayout from '@/layouts/account';
import PasswordChangeContainer from '@/containers/password';
import AuthLayout from '@/layouts/auth';

export default function CampaignPage() {
  return (
    <>
      <Head>
        <title>Ubah Password</title>
      </Head>
      <MainLayout>
        <AccountLayout parent="profil">
          <AuthLayout>
            <PasswordChangeContainer />
          </AuthLayout>
        </AccountLayout>
      </MainLayout>
    </>
  );
}
