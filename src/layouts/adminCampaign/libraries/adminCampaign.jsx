import AdminLayout from '@/layouts/admin';
import AdminCampaignContainer from '@/containers/admin/campaign/detail';

// eslint-disable-next-line react/prop-types
export default function AdminCampaignLayout({ children }) {
  return (
    <>
      <AdminLayout>
        <div className="bg-white p-10 border">
          <AdminCampaignContainer />
          {children}
        </div>
      </AdminLayout>
    </>
  );
}
