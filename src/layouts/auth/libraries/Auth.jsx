import { useEffect, useState } from 'react';

export function withAuth(WrappedComponent) {
  const Content = () => {
    const [user] = useState(
      typeof window !== 'undefined' ? JSON.parse(localStorage.getItem('token')) : '',
    );

    useEffect(() => {
      if (!user) {
        window.location.href = '/login';
      }
    }, [user]);
    return (
      <WrappedComponent />
    );
  };

  return Content;
}

// export default function withAuth() {
//   const [user] = JSON.parse(localStorage.getItem('user'));

//   if (user && user.accessToken) {
//     return { Authorization: 'Bearer ' + user.accessToken };
//   } else {
//     return {};
//   }
// }

export function withNoAuth(WrappedComponent) {
  const Content = () => {
    const [user] = useState(
      typeof window !== 'undefined' ? JSON.parse(localStorage.getItem('token')) : '',
    );
    // const [show, setShow] = useState(false);

    useEffect(() => {
      if (user) {
        window.location.href = '/'; // dashboard
      }
    }, [user]);
    return (
      <WrappedComponent />
    );
  };
  return Content;
}

// eslint-disable-next-line
export default function AuthLayout({ children }) {
  return (
    { ...children }
  );
}
