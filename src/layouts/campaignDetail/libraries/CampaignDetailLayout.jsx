import Main from '@/layouts/main';
import CampaignDetailContainer from '@/containers/campaign/Detail';

// eslint-disable-next-line react/prop-types
export default function MonitorLayout({ children }) {
  return (
    <>
      <Main>
        <div className="container mx-auto flex flex-col gap-5  border-black p-12">
          <CampaignDetailContainer />
          {children}
        </div>
      </Main>
    </>
  );
}
