/* eslint-disable max-len */
/* eslint-disable no-console */
import { Formik } from 'formik';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import { useUserDispatch } from '@/redux/reducers/user/slices';
import { sessionUser } from '@/helpers/auth';
import { baseApiUrl } from '@/helpers/baseContents';
import { withAuth } from '@/layouts/auth/libraries/Auth';

function ProfileContainer() {
  const { user, getUserDetail } = useUserDispatch();
  const { userDetail } = user;
  const token = sessionUser();
  const [preview, setPreview] = useState();
  const [loading, setLoading] = useState();
  const [image, setImage] = useState('');

  useEffect(() => {
    if (userDetail) {
      getUserDetail(token);
    }
  }, []);

  const uploadProfile = async (values) => {
    const formData = new FormData();
    formData.append('file', values.imgUrl);
    console.log('file', values.imgUrl);

    try {
      const response = await fetch(`${baseApiUrl}/file/upload`, {
        method: 'POST',
        body: formData,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      console.log(data);
      const fileUri = await data.fileDownloadUri;
      setImage(fileUri);
      return fileUri;
    } catch (error) {
      console.error(error);
    }
  };

  const updateProfile = async (values) => {
    const {
      fullName, phone, username, imgUrl,
    } = values;
    // console.log(values);
    const formData = {
      fullName,
      phone,
      username,
      imgUrl,
    };

    try {
      const response = await fetch(`${baseApiUrl}/user/update`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  async function handleOnSubmit(values) {
    setLoading(true);
    const imgUrl = await uploadProfile(values);
    await updateProfile({ ...values, imgUrl });
    setLoading(false);
    window.location.reload();
  }

  useEffect(() => {
  // storing input
    localStorage.setItem('imgUrl', JSON.stringify(image));
  }, [image]);

  return (
    <div className="w-3/4">
      {/* navbar profil */}
      <div className="flex flex-row container w-full justify-center mb-24">
        <div className="w-full mx-auto">
          <div className="border border-gray-150 rounded-md p-5">
            <div className="border-b border-gray-250">
              <div className="text-lg font-semibold">
                Profil Saya
              </div>
              <div className="text-lg mt-2 mb-1">
                Kelola informasi profil Anda.
              </div>
            </div>
            {userDetail && userDetail.id ? (
              <Formik
                initialValues={{
                  fullName: userDetail.fullName,
                  username: userDetail.username,
                  phone: userDetail.phone,
                  imgUrl: userDetail.imgUrl,
                }}
                onSubmit={(values) => handleOnSubmit(values)}
                enableReinitialize
              >
                {({
                  handleSubmit,
                  handleChange,
                  handleBlur,
                  setFieldValue,
                }) => (
                  <form onSubmit={handleSubmit}>
                    <div className="flex flex-row pt-5 pb-2">
                      <div className="w-3/4 border-gray-250 pr-8 text-lg border-r">
                        <div className="pb-5">
                          <label htmlFor="fullName" className="flex justify-between gap-10">
                            <span className="w-60">Nama</span>
                            <input
                              type="text"
                              name="fullName"
                              id="fullName"
                              // placeholder={userDetail.fullName}
                              defaultValue={userDetail.fullName}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              className="border border-gray-250 rounded-sm pl-2 w-full focus:outline-none focus:border-green-250"
                            />
                          </label>
                        </div>
                        <div className="py-5">
                          <label htmlFor="username" className="flex justify-between gap-10">
                            <span className="w-60">Email</span>
                            <input
                              type="text"
                              name="username"
                              id="username"
                              // placeholder={userDetail.username}
                              defaultValue={userDetail.username}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              className="border border-gray-250 rounded-sm pl-2 w-full focus:outline-none focus:border-green-250"
                            />
                          </label>
                        </div>
                        <div className="pt-5">
                          <label htmlFor="phone" className="flex justify-between gap-10">
                            <span className="w-60">Nomor Telepon</span>
                            <input
                              type="text"
                              name="phone"
                              id="phone"
                              // placeholder={userDetail.phone}
                              defaultValue={userDetail.phone}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              className="border border-gray-250 rounded-sm pl-2 w-full focus:outline-none focus:border-green-250"
                            />
                          </label>
                        </div>
                      </div>
                      <div className="w-1/4">
                        {/* foto */}
                        <div className="flex flex-col items-center">
                          <div>
                            {preview ? (
                              <Image src={preview} width="125" height="125" alt="foto profil" className="border-none rounded-xl" />
                            ) : (
                              <Image src={userDetail && userDetail.imgUrl ? userDetail.imgUrl : '/img/profile/light-gray.png'} width="125" height="125" alt="foto profil" className="border-none rounded-xl" />
                            )}
                          </div>
                          <label htmlFor="imgUrl" className="border rounded-md py-2 px-4 cursor-pointer hover:bg-green-250 hover:text-white">
                            Pilih Gambar
                            <input
                              type="file"
                              id="imgUrl"
                              name="imgUrl"
                              className="hidden"
                              onChange={(event) => {
                                if (event.target.files.length !== 0) {
                                  setFieldValue('imgUrl', event.currentTarget.files[0]);
                                  const { files } = event.target;
                                  const tempURL = URL.createObjectURL(files[0]);
                                  setPreview(tempURL);
                                }
                              }}
                              accept=".PNG, .JPEG, .JPG"
                            />
                          </label>

                          <div className="text-xs text-gray-225 pt-2">Ukuran gambar: maks 2 MB</div>
                          <div className="text-xs text-gray-225">Format Gambar: .JPEG, .PNG</div>

                        </div>
                      </div>
                    </div>
                    <div className="pb-20 ml-52">
                      <button type="submit" className="rounded-lg bg-green-250 text-white border-none py-2 px-4">
                        {loading ? 'Mohon Tunggu...' : 'Simpan'}
                      </button>
                    </div>
                  </form>
                )}
              </Formik>
            ) : <div className="text-center items-center flex">Mohon Tunggu...</div>}
          </div>
        </div>
      </div>
    </div>
  );
}

export default withAuth(ProfileContainer);
