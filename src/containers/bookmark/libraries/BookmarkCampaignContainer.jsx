import Image from 'next/image';
// import { useRouter } from 'next/router';
import { useEffect } from 'react';
// import Link from 'next/link';
import ReactPaginate from 'react-paginate';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import CampaignGridView from '@/components/CampaignGridView';
import { sessionUser } from '@/helpers/auth.js';
import { withAuth } from '@/layouts/auth/libraries/Auth';
import LinkButton from '@/components/LinkButton';

function BookmarkCampaignContainer() {
  const token = sessionUser();
  const {
    fetchBookmarks, campaign,
  } = useCampaignDispatch();
  const {
    bookmarks, loading, totalPages, page,
  } = campaign;

  const renderBookmarks = () => bookmarks && bookmarks.map((item) => (
    <div key={`campaign-${item.id}`}>
      <CampaignGridView {...item} />
    </div>
  ));

  const handlePageChange = async (selectedPage) => {
    await fetchBookmarks({ token, page: selectedPage.selected });
  };

  useEffect(async () => {
    await fetchBookmarks({ token });
    // await fetchCampaignDetail();
  }, []);

  return (
    <>
      <div className="container mx-auto">
        {bookmarks && bookmarks.length > 0 && (
          <div className="flex justify-center items-center">
            <div className="grid grid-cols-3 gap-16 mx-8 my-8">
              {renderBookmarks()}
            </div>
          </div>

        )}
        {/* {loading === campaign && campaign[0].id ? (
          <div className="flex justify-center items-center">loading</div>
        ) : (
          <div className="grid grid-cols-3 gap-16">
            {renderBookmarks(item)}
          </div>
        )} */}

        {loading === false && bookmarks && bookmarks.length === 0 ? (
          <div>
            <div className="flex justify-center items-center mt-5">
              <Image
                src="/img/campaign/boxes.png"
                width="311"
                height="319"
                alt="empty-boxes"
              />
            </div>
            <div className="flex justify-center items-center mt-8">
              Belum ada campaign yang disimpan
            </div>
            <div className="flex justify-center items-center my-4">
              <LinkButton url="/campaign" text="Mulai Cari Campaign" />
            </div>
          </div>

        ) : (
          <div className="flex justify-center items-center my-4">
            <ReactPaginate
              containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
              activeClassName="border border-green-250 px-2 rounded-md"
              initialPage={page}
              pageCount={totalPages}
              previousLabel="<"
              nextLabel=">"
              onPageChange={(e) => handlePageChange(e)}
            />
          </div>
        )}
      </div>
    </>
  );
}

export default withAuth(BookmarkCampaignContainer);
