/* eslint-disable no-console */
import { useEffect } from 'react';
import { sessionUser } from '@/helpers/auth';
import { useUserDispatch } from '@/redux/reducers/user/slices';
import { baseApiUrl } from '@/helpers/baseContents';
import { withAuth } from '@/layouts/auth/libraries/Auth';
import Address from '@/components/PopupAddress';
import EditAddress from '@/components/PopupAddressEdit';
import DeleteAddress from '@/components/PopupDeleteAddress';

function AddressListContainer() {
  const token = sessionUser();

  const { getAllAddress, user } = useUserDispatch();
  const { addresses } = user;

  useEffect(() => {
    if (addresses) {
      getAllAddress();
      console.log(addresses);
    }
  }, []);

  const handleRemove = async (id) => {
    const response = await fetch(`${baseApiUrl}/address/delete/${id}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await response.json();
    console.log(data);
    window.location.reload();
  };

  const setMainAddress = async (id) => {
    try {
      const response = await fetch(`${baseApiUrl}/address/main/${id}`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      console.log(data);
      window.location.reload();
    } catch (error) {
      console.log(error);
    }
  };

  const renderAddress = () => addresses && addresses.map((address) => (
    <div key={address.id} className="flex flex-col gap-3 border-b-2 pb-10 my-2 break-words">
      <div className="grid grid-cols-4">
        <div className="col-span-3">
          <div className="grid grid-cols-5">
            <div className="col-span-1">
              Nama
            </div>
            <div className="col-span-4 font-semibold flex">
              {address.recipient}
              {address.main === true ? (
                <div className="flex rounded-full bg-green-250 text-white ml-3 px-2">
                  Utama
                </div>
              ) : null}
            </div>
          </div>
          <div className="grid grid-cols-5">
            <div className="col-span-1">
              Telepon
            </div>
            <div className="col-span-4">
              {address.phone}
            </div>
          </div>
          <div className="grid grid-cols-5">
            <div className="col-span-1">
              Alamat
            </div>
            <div className="col-span-4">
              <div>
                {address.address}
              </div>
              <div>
                {address.postalCode}
              </div>
            </div>
          </div>
        </div>
        <div className="col-span-1">
          <div className="grid grid-cols-2 text-right pr-4 pt-4">
            <div className="col-span-1">
              <EditAddress addressId={address.id} modifyModal {...address} />
            </div>
            {address.main ? (
              <div className="col-span-1 text-md underline text-gray-400">Hapus</div>
            ) : (
              <DeleteAddress onConfirm={() => handleRemove(address.id)} />
            )}
          </div>
          {address.main === true ? (
            <button className="border text-gray-325 p-2 float-right" disabled type="button">
              Atur Sebagai Utama
            </button>
          ) : (
            <button className="border border-gray-350 p-2 float-right" type="button" onClick={() => setMainAddress(address.id)}>
              Atur Sebagai Utama
            </button>
          )}
        </div>
      </div>
    </div>
  ));

  return (
    <div className="w-3/4 border border-gray-150 rounded-md px-5 overflow-y-auto" style={{ height: 1000 }}>
      <div className="flex border-b-2 border-gray-150 pb-2 sticky top-0 bg-white p-5">
        <div className="text-xl flex flex-grow flex-col gap-1">
          <div className="font-semibold">Alamat Saya</div>
          <div>Kelola Alamat Anda</div>
        </div>
        <div className="flex">
          <Address modifyModal />
        </div>
      </div>
      <div className="flex flex-col p-5 gap-3">

        {/* {loading ? 'loading' : renderAddress()} */}
        {renderAddress()}
      </div>
    </div>
  );
}

export default withAuth(AddressListContainer);
