import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import CampaignListView from '@/components/CampaignListView';
import NavbarMonitor from '@/components/NavbarMonitor';
import { useDonationDispatch } from '@/redux/reducers/donation/slices';
import Breadcrumb from '@/components/Breadcrumb';
// import { sessionUser } from '@/helpers/auth';

export default function CampaignMonitorContainer() {
  const router = useRouter();
  // const token = sessionUser();
  const { campaignId } = router.query;
  const {
    fetchCampaignDetail, fetchRewards, campaign,
  } = useCampaignDispatch();
  const { campaignDetail } = campaign;
  const { fetchDonation } = useDonationDispatch();

  useEffect(() => {
    if (campaignId) {
      fetchCampaignDetail(campaignId);
      fetchRewards(campaignId);
      fetchDonation({ campaignId });
    }
  }, [campaignId]);

  const pages = [
    { name: 'Campaign Saya', href: '/mycampaign', current: false },
    { name: 'Monitor Campaign', href: '#', current: true },
  ];

  return (
    <div className="container mx-auto p-8">
      <div className="-ml-4 mb-4">
        <Breadcrumb pages={pages} />
      </div>

      {
        campaignDetail && campaignDetail.id ? (<CampaignListView monitoring {...campaignDetail} />) : 'loading'
      }
      <NavbarMonitor />
    </div>
  );
}
