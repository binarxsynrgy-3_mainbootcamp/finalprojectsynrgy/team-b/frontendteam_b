import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import CampaignGridView from '@/components/CampaignGridView';
import Reward from '@/components/Reward';
import LinkButton from '@/components/LinkButton';
import { sessionUser } from '@/helpers/auth';
import { baseReward } from '@/helpers/baseContents';

export default function CampaignInfoDetailContainer() {
  const router = useRouter();
  const { campaignId } = router.query;
  const { campaign } = useCampaignDispatch();
  const { campaigns, campaignDetail, rewards } = campaign;

  const renderDetail = (item) => (
    <div className="flex items-center justify-center">
      <div className="flex flex-col text-gray-650 border-b-2 border-gray-150 p-5" style={{ width: 1032 }}>
        <div className="font-semibold text-5xl">{item.title}</div>
        <div className="flex justify-center my-10">
          {item.imgUrl ? <Image src={item.imgUrl} className="object-contain" width="1032" height="638" /> : <EmptyImage w={1032} h={638} source="default" />}
        </div>

        <div className="text-xl text-justify">{item.description}</div>
      </div>
    </div>

  );

  const renderRewards = () => rewards && rewards.map((item) => (
    <Reward {...item} key={`reward-${item.id}`} />
  ));

  const renderSuggestion = () => campaigns && campaigns.map((item) => (
    <Link href={`/campaign/${item.id}/detail`} key={`suggestion-${item.id}`}>
      <div className={String(item.id) === String(campaignId) ? 'hidden' : 'cursor-pointer'}>
        <CampaignGridView detailed={false} {...item} />
      </div>
    </Link>
  ));

  return (
    <>
      <div className="text-xl text-green-550 font-semibold text-center"> Cerita </div>

      {campaignDetail && campaignDetail.id ? renderDetail(campaignDetail) : <div className="text-center">loading</div>}

      <div className="text-xl text-green-550 font-semibold text-center my-5"> Penawaran Hadiah Donasi </div>

      <div className="border-b-2 border-gray-150 pb-10 flex flex-col items-center gap-10">
        <div className="flex gap-10 justify-center ">
          <Reward {...baseReward} />
          { renderRewards() }
        </div>
        <div className="flex gap-5 w-full justify-center">
          {sessionUser() ? (
            <LinkButton url={`/campaign/${campaignId}/donation`} text="Donasi Sekarang" />
          )
            : (
              <>
                <LinkButton url={`/campaign/${campaignId}/donation`} text="Donasi Tanpa Login" />
                <LinkButton url={`/login?campaign=${campaignId}`} text="Login dan Dapatkan Paket" />
              </>
            )}

        </div>
      </div>

      <div className="text-2xl font-semibold text-center">
        Mereka juga
        {' '}
        <span className="text-green-250">butuh</span>
        {' '}
        dukunganmu
      </div>

      <div className="flex gap-16 overflow-x-auto pb-10">
        { renderSuggestion() }
      </div>
    </>

  );
}
