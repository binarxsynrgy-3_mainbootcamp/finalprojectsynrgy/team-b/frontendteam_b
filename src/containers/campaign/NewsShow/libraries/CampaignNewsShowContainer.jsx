import Image from 'next/image';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import ReactPaginate from 'react-paginate';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import EmptyImage from '@/components/EmptyImage';
import { convertDateToString } from '@/helpers/dateTime';

export default function CampaignNewsShowContainer() {
  const router = useRouter();
  const { campaignId } = router.query;
  const { campaign, fetchNews } = useCampaignDispatch();
  const { news, page, totalPages } = campaign;

  useEffect(() => {
    if (campaignId) {
      fetchNews({ campaignId });
    }
  }, [campaignId]);

  const handlePageChange = async (selectedPage) => {
    await fetchNews({ campaignId, page: selectedPage.selected });
  };

  const renderNews = () => news && news.map((item) => (
    <div className="flex flex-col border-b-2 p-5" key={item.id}>
      <div>
        {convertDateToString(item.updatedAt)}
      </div>
      <div className="flex flex-col text-gray-650 border-gray-150 p-8 gap-5">
        <div className="font-semibold text-5xl">{item.title}</div>
        <div className="flex justify-center my-10">
          {item.imgUrl ? <Image src={item.imgUrl} width="1032" height="638" /> : <div className="flex items-center justify-center" style={{ width: 1032, height: 638 }}>no image found</div>}
        </div>

        <div className="text-xl text-justify">{item.activity}</div>
      </div>
    </div>

  ));

  return (
    <>
      {news && news.length > 0 ? renderNews() : (
        <div className="flex flex-col justify-center items-center p-4 gap-5">
          <EmptyImage source="campaign" h={320} w={320} />
          <div>Belum ada update terbaru untuk campaign ini</div>
        </div>
      )}

      {totalPages <= 1 ? null : (
        <ReactPaginate
          containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
          activeClassName="border border-green-250 px-2 rounded-md"
          initialPage={page}
          pageCount={totalPages}
          previousLabel="<"
          nextLabel=">"
          onPageChange={(e) => handlePageChange(e)}
        />
      )}
    </>

  );
}
