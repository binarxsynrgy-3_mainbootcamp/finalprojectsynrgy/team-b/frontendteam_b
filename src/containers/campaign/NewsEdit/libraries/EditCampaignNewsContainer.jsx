import { Formik, Form } from 'formik';
import { useEffect, useState } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { baseApiUrl } from '@/helpers/baseContents';
import { sessionUser } from '@/helpers/auth';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import CreatorNewsForm from '@/components/CreatorNewsForm';
import Breadcrumb from '@/components/Breadcrumb';

export default function CreateCampaignNewsContainer() {
  const router = useRouter();
  const { campaignId, newsId } = router.query;
  const [preview, setPreview] = useState();
  const [fileData, setFileData] = useState('');
  const [loading, setLoading] = useState();

  const {
    fetchCampaignDetail, fetchNewsDetail, campaign,
  } = useCampaignDispatch();
  const { campaignDetail, newsDetail } = campaign;

  const token = sessionUser();

  useEffect(() => {
    if (campaignId) {
      fetchCampaignDetail(campaignId);
    }
  }, [campaignId]);

  useEffect(() => {
    if (newsId) {
      fetchNewsDetail(newsId);
    }
  }, [newsId]);

  useEffect(() => {
    if (newsDetail && newsDetail.imgUrl) {
      setFileData(newsDetail.imgUrl);
    }
  }, [newsDetail]);

  const pages = [
    { name: 'Kabar Terbaru', href: `/mycampaign/${campaignId}/news`, current: false },
    { name: 'Edit Kabar Terbaru', href: '#', current: true },
  ];

  const handleUploadImage = async (values) => {
    const formData = new FormData();
    formData.append('file', values.imgUrl);
    if (token) {
      try {
        const response = await fetch(`${baseApiUrl}/file/upload`, {
          method: 'POST',
          body: formData,
        });
        const data = await response.json();
        const fileUri = await data.fileDownloadUri;
        return fileUri;
        // setFileData(fileUri);

        // console.log('fileUri = ', fileUri);
        // console.log('fileData state = ', fileData);
        // console.log(data);
      } catch (error) {
        console.log(error);
      }
    }
    return null;
  };

  const submitForm = async (values) => {
    // if (newsDetail && newsDetail.imgUrl) {
    //   setFileData(newsDetail.imgUrl);
    // }

    const {
      title, activity, imgUrl,
    } = values;

    const formData = {
      title,
      activity,
      imgUrl,
      campaignId,
      id: newsId,
    };

    try {
      const response = await fetch(`${baseApiUrl}/history/update/`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleOnSubmit = async (values) => {
    setLoading(true);
    const imgUrl = await handleUploadImage(values);
    await submitForm({ ...values, imgUrl });
    setLoading(false);
    router.push(`/mycampaign/${campaignId}/news`);
    // redirect ke list news
  };

  return (
    <div className="w-3/4 border border-gray-150 rounded-md p-5 overflow-y-auto" style={{ height: 1000 }}>
      <div className="text-xl border-b-2 border-gray-150 pb-2 flex flex-col gap-1">
        <div className="-ml-4">
          <Breadcrumb pages={pages} />
        </div>
        <div>{campaignDetail && campaignDetail.title ? campaignDetail.title : 'Judul Campaign'}</div>
      </div>
      <div>
        {newsDetail && newsDetail.title ? (
          <CreatorNewsForm
            campaignId={campaignId}
            loading={loading}
            onSubmit={handleOnSubmit}
            {...newsDetail}
          />
        ) : (<div className="ml-7 mt-7">loading</div>)}

      </div>
    </div>
  );
}
