/* eslint-disable no-nested-ternary */
import { useRouter } from 'next/router';
import ReactPaginate from 'react-paginate';
import Image from 'next/image';
import { useDonationDispatch } from '@/redux/reducers/donation/slices';
import { convertDateToString } from '@/helpers/dateTime';
import EmptyImage from '@/components/EmptyImage';
// import { sessionUser } from '@/helpers/auth';

export default function CampaignMonitorDonationContainer() {
  const router = useRouter();
  // const token = sessionUser();
  const { campaignId } = router.query;
  const { donation, fetchDonation } = useDonationDispatch();
  const {
    donations, totalPages, page, loading,
  } = donation;

  const handlePageChange = async (selectedPage) => {
    await fetchDonation({ campaignId, page: selectedPage.selected });
  };

  const renderDonation = () => donations && donations.map((item) => (
    <>
      <div className="flex gap-3 p-5 rounded-lg text-xl border border-gray-150">
        <div className="p-3">
          {item.profileImage && item.profileFullName ? <Image src={item.profileImage} width="106" height="106" /> : <EmptyImage h={106} w={106} source="profile" />}
        </div>

        <div className="flex flex-col gap-6 font-semibold w-full">
          <div className="h-20">
            <div>
              {item.profileFullName ? item.profileFullName : 'Anonim'}
            </div>
            <div className="font-normal">
              {item.notes}
            </div>
          </div>

          <div className="flex flex-grow justify-between w-1/2">
            <div className="font-normal">{convertDateToString(item.updatedAt)}</div>
            <div>
              Rp.
              {' '}
              {item.amount && (item.amount).toLocaleString('id-ID')}
              ,-
              {' '}
            </div>
          </div>
        </div>
      </div>

    </>

  ));

  return (
    <div className="container mx-auto p-8">
      <div>
        {loading ? 'loading' : (
          donations && donations.length === 0 ? (
            'tidak ada data'
          ) : (
            <div className="flex flex-col gap-8">
              {renderDonation()}
            </div>
          )
        )}
        {' '}
        <div className={`${totalPages <= 1 && 'hidden'} flex justify-center`}>
          <ReactPaginate
            containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
            activeClassName="border border-green-250 px-2 rounded-md"
            initialPage={page}
            pageCount={totalPages}
            previousLabel="<"
            nextLabel=">"
            onPageChange={(e) => handlePageChange(e)}
          />
        </div>

      </div>

    </div>
  );
}
