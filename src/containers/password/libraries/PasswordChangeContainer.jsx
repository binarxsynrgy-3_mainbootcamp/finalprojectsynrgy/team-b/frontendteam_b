/* eslint-disable max-len */
/* eslint-disable no-console */
import { Formik, Form } from 'formik';
import { useState } from 'react';
import * as Yup from 'yup';
import PopupForgot from '@/components/PopupForgot';
import { sessionUser } from '@/helpers/auth';
import { baseApiUrl } from '@/helpers/baseContents';
import { withAuth } from '@/layouts/auth/libraries/Auth';

const initialValues = {
  oldPassword: '',
  newPassword: '',
  newPasswordConf: '',
};

const validationSchema = Yup.object({
  oldPassword: Yup.string()
    .required('This must be filled'),
  newPassword: Yup.string()
    .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, 'Password must contain at least 8 characters and at least one number and one capital letter')
    .required('This must be filled'),
  newPasswordConf: Yup.string()
    .oneOf([Yup.ref('newPassword'), null], 'Password must match'),
});

function PasswordChangeContainer() {
  const token = sessionUser();
  const [loading, setLoading] = useState();

  // const updatePassword = async (values) => {
  //   try {
  //     const response = await fetch(`${baseApiUrl}/user/update-password`, {
  //       method: 'PUT',
  //       headers: {
  //         Authorization: `Bearer ${token}`,
  //         'Content-Type': 'application/json',
  //       },
  //       body: JSON.stringify(values),
  //     });
  //     const data = await response.json();
  //     console.log(data);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  async function handleOnSubmit(values) {
    setLoading(false);
    try {
      const response = await fetch(`${baseApiUrl}/user/update-password`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(values),
      });
      const data = await response.json();
      console.log(data);
      setLoading(true);
      window.location.reload();
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
    setLoading(false);
  }

  return (
    <div className="w-3/4">
      {/* navbar profil */}
      <div className="flex flex-row container w-full justify-center mb-24">
        <div className="w-full mx-auto">
          <div className="border border-gray-150 rounded-md p-5">
            <div className="border-b border-gray-250">
              <div className="text-lg font-semibold">
                Ubah Password
              </div>
              <div className="text-lg mt-2 mb-1">
                Untuk keamanan akun Anda, mohon untuk tidak menyebarkan password Anda.
              </div>
            </div>
            <Formik
              onSubmit={(values) => handleOnSubmit(values)}
              initialValues={initialValues}
              validationSchema={validationSchema}
            >
              {({
                handleSubmit,
                handleChange,
                handleBlur,
                touched,
                errors,
                isValid,
                dirty,
              }) => (
                <Form onSubmit={handleSubmit}>
                  <div className="flex flex-row pt-5 pb-2">
                    <div className="w-3/4 border-gray-250 pr-6 text-lg border-r">
                      <div className="pb-5">
                        <label htmlFor="oldPassword" className="flex justify-between gap-10">
                          <span className="w-72">Password Saat Ini</span>
                          <input
                            type="text"
                            name="oldPassword"
                            id="oldPassword"
                              // defaultValue={userDetail.fullName}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className="border border-gray-250 rounded-sm pl-2 w-full focus:outline-none focus:border-green-250"
                          />
                        </label>
                        <div className={`box-border py-1 pb-2 text-sm ml-60 ${touched.oldPassword && errors.oldPassword ? 'text-red-700' : ''}`}>
                          {touched.oldPassword && errors.oldPassword && errors.oldPassword}
                        </div>
                      </div>
                      <div className="py-5">
                        <label htmlFor="newPassword" className="flex justify-between gap-10">
                          <span className="w-72">Password Baru</span>
                          <input
                            type="text"
                            name="newPassword"
                            id="newPassword"
                              // defaultValue={userDetail.username}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className="border border-gray-250 rounded-sm pl-2 w-full focus:outline-none focus:border-green-250"
                          />
                        </label>
                        <div className={`box-border py-1 pb-2 text-sm ml-60 ${touched.newPassword && errors.newPassword ? 'text-red-700' : ''}`}>
                          {touched.newPassword && errors.newPassword && errors.newPassword}
                        </div>
                      </div>
                      <div className="pt-5">
                        <label htmlFor="newPasswordConf" className="flex justify-between gap-10">
                          <span className="w-72">Konfirmasi Password</span>
                          <input
                            type="text"
                            name="newPasswordConf"
                            id="newPasswordConf"
                              // defaultValue={userDetail.phone}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className="border border-gray-250 rounded-sm pl-2 w-full focus:outline-none focus:border-green-250"
                          />
                        </label>
                        <div className={`box-border py-1 pb-2 text-sm ml-60 ${touched.newPasswordConf && errors.newPasswordConf ? 'text-red-700' : ''}`}>
                          {touched.newPasswordConf && errors.newPasswordConf && errors.newPasswordConf}
                        </div>
                      </div>
                    </div>
                    <div className="w-1/4">
                      <div className="flex flex-col items-center">
                        <PopupForgot />
                      </div>
                    </div>
                  </div>
                  <div className="pb-20 ml-60 mt-8">
                    <button type="submit" className="rounded-lg bg-green-250 text-white border-none py-2 px-4" disabled={!(isValid && dirty) && loading}>
                      {loading ? 'Mohon Tunggu...' : 'Konfirmasi'}
                    </button>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
}

export default withAuth(PasswordChangeContainer);
