import Image from 'next/image';
import { useEffect } from 'react';
// import Link from 'next/link';
import ReactPaginate from 'react-paginate';
import moment from 'moment';
import 'moment/locale/id';
import { useDonationDispatch } from '@/redux/reducers/donation/slices';
import { sessionUser } from '@/helpers/auth.js';
import PopupHistory from '@/components/PopupHistory';
import { withAuth } from '@/layouts/auth/libraries/Auth';
import LinkButton from '@/components/LinkButton';
import EmptyImage from '@/components/EmptyImage';
// import { convertDateToString } from '@/helpers/dateTime.js';

function HistoryCampaignContainer() {
  const token = sessionUser();
  const {
    donation, fetchHistory,
  } = useDonationDispatch();
  const {
    history, loading, totalPages, page,
  } = donation;

  const renderHistory = () => history && history.map((item) => (
    <div className="flex justify-center">
      <div className="border rounded-md h-auto w-10/12 my-3">
        <div className="grid grid-cols-8 my-3 ml-4">
          <div className="col-span-1 flex justify-start items-center">
            {item.campaignImage ? <Image src={item.campaignImage} width={106} height={106} className="object-cover" /> : <EmptyImage h={106} w={106} source="default" />}
          </div>
          <div className="col-span-5 flex justify-start items-center">
            <div>
              <div className="font-semibold">
                <div>{item.campaignTitle}</div>
              </div>
              <div className="flex justify-start items-center gap-16">
                <div className="mt-6">
                  {moment(item.createdAt).format('LL')}
                </div>
                <div className="font-semibold mt-6">
                  Rp
                  {' '}
                  {(item.amount).toLocaleString('id-ID')}
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-2 flex justify-center items-center gap-2">
            <PopupHistory {...item} />
          </div>
        </div>
      </div>
    </div>
  ));

  const handlePageChange = async (selectedPage) => {
    // await fetchDonation({ token, page: selectedPage.selected });
    await fetchHistory({ token, page: selectedPage.selected });
  };

  useEffect(async () => {
    // await fetchDonation({ token });
    await fetchHistory({ token });
  }, []);

  return (
    <>
      <div className="container text-xl mx-auto mt-5 px-10">
        <div className="font-semibold">Riwayat Donasi</div>
        <div className="bg-gray-150 h-0.5 w-auto my-4" />
        {loading ? (
          <div className="flex justify-center items-center">loading</div>
        ) : (
          <div className="">
            {renderHistory()}
          </div>
        ) }
        {loading === false && history && history.length === 0 ? (
          <div>
            <div className="flex justify-center items-center">
              <Image
                src="/img/campaign/boxes.png"
                width="311"
                height="319"
                alt="empty-boxes"
              />
            </div>
            <div className="flex justify-center items-center mt-8">
              Belum ada donasi yang dilakukan
            </div>
            <div className="flex justify-center items-center my-4">
              <LinkButton url="/campaign" text="Donasi Sekarang" />
            </div>
          </div>

        ) : (
          <div className="flex justify-center my-4">
            <ReactPaginate
              containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
              activeClassName="border border-green-250 px-2 rounded-md"
              initialPage={page}
              pageCount={totalPages}
              previousLabel="<"
              nextLabel=">"
              onPageChange={(e) => handlePageChange(e)}
            />
          </div>

        )}
      </div>
    </>
  );
}

export default withAuth(HistoryCampaignContainer);
