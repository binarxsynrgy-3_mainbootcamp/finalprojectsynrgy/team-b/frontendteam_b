import Image from 'next/image';

export default function AboutContainer() {
  return (
    <>
      <div className="mx-auto container">
        {/* Section 1 */}
        <div className="my-8 mx-6">
          <div className="flex items-center justify-center my-16">
            <h1 className="text-5xl text-green-550 font-semibold">Tentang Kami</h1>
          </div>
          <div className="grid grid-cols-3">
            <div className="col-span-2 flex items-center justify-center ml-16 mr-32">
              <p className="font-bold text-4xl leading-normal">
                <span className="text-green-550">Asa</span>
                <span className="text-green-250">Karya</span>
                {' '}
                merupakan suatu platform socinvest crowdfunding yang berfokus pada industri kreatif.
              </p>
            </div>
            <div className="col-span-1 flex items-center justify-center">
              <Image
                src="/img/about/biglogo.png"
                alt="logo"
                width={411}
                height={588}
              />
            </div>
          </div>
          <div className="bg-gray-150 h-0.5 my-6 mx-1 mt-12" />

          {/* Section 2 */}
          <div className="flex items-center justify-center my-16">
            <h1 className="text-5xl text-green-550 font-semibold">Mengapa Asakarya Hadir</h1>
          </div>
          <div className="grid grid-cols-4">
            <div className="col-span-3 ml-16">
              <h1 className="font-bold text-4xl">
                Sejarah Kami
              </h1>
              <p className="font-normal text-2xl mt-4">
                Asakarya didirikan pada tahun 2021 di Indonesia.
              </p>
              <p className="font-normal text-2xl justify-start">
                Berisi orang-orang yang berasal dari berbagai latar belakang
                namun memiliki satu Visi dan Misi yang sama untuk membangun
                {' '}
                <span className="text-green-550">Asa</span>
                <span className="text-green-250">Karya</span>
              </p>
            </div>
            <div className="col-span-1 flex justify-end">
              <Image
                src="/img/about/history.png"
                alt="sejarah"
                width={250}
                height={225}
              />
            </div>
          </div>
          <div className="ml-16">
            <h1 className="font-bold text-4xl mt-8">
              Latar Belakang
            </h1>
            <p className="font-normal text-2xl mt-4">
              Indonesia memiliki segudang pelaku industri kreatif yang hebat, namun sayangnya banyak
              dari mereka gagal mengembangkan karyanya karena keterbatasan dana. Melalui
              {' '}
              <span className="text-green-550">Asa</span>
              <span className="text-green-250">Karya</span>
              , kami
              hadir sebagai
              wadah kemajuan Industri Kreatif di Indonesia.
            </p>
            <p className="font-normal text-2xl mt-8">
              Bersama
              {' '}
              <span className="text-green-550">Asa</span>
              <span className="text-green-250">Karya</span>
              , kita dukung karya anak bangsa!
            </p>
          </div>
          <div className="bg-gray-150 h-0.5 my-6 mx-1 mt-12" />

          {/* Section 3 */}
          <div className="flex items-center justify-center my-16">
            <h1 className="text-5xl text-green-550 font-semibold">Fokus Kami</h1>
          </div>
          <div className="grid grid-cols-3">
            <div className="col-span-1 flex items-center justify-center">
              <Image
                src="/img/about/focus.png"
                alt="fokus"
                width={449}
                height={393}
              />
            </div>
            <div className="col-span-2 flex items-center justify-center">
              <p className="font-bold text-4xl leading-normal mr-8 ml-32">
                Saat ini
                {' '}
                <span className="text-green-550">Asa</span>
                <span className="text-green-250">Karya</span>
                {' '}
                berfokus sebagai media penghubung antara pelaku
                industri kreatif dengan investor untuk #dukungkaryaanakbangsa
              </p>
            </div>
          </div>
          <div className="bg-gray-150 h-0.5 my-6 mx-1 mt-12" />

          {/* Section 4 */}
          <div className="flex items-center justify-center my-16">
            <h1 className="text-5xl text-green-550 font-semibold">Dibalik Asakarya</h1>
          </div>
          <div className="grid grid-cols-4 gap-8 justify-content-center">
            <div>
              <div className="flex items-center justify-center my-2">
                <Image
                  className="rounded-3xl"
                  src="/img/about/fauzi.jpg"
                  alt="profile"
                  width={133}
                  height={136}
                />
              </div>
              <div>
                <p className="flex items-center justify-center my-2">Fauzi Amallul</p>
                <p className="flex items-center justify-center my-2">PM dan UI/UX Design</p>
              </div>
            </div>
            <div>
              <div className="flex items-center justify-center my-2">
                <Image
                  className="rounded-3xl"
                  src="/img/about/hisyam.jpeg"
                  alt="profile"
                  width={133}
                  height={136}
                />
              </div>
              <div>
                <p className="flex items-center justify-center my-2">Hisyam Naufaldi</p>
                <p className="flex items-center justify-center my-2">UI/UX Design</p>
              </div>
            </div>
            <div>
              <div className="flex items-center justify-center my-2">
                <Image
                  className="rounded-3xl"
                  src="/img/about/lulu.png"
                  alt="profile"
                  width={133}
                  height={136}
                />
              </div>
              <div>
                <p className="flex items-center justify-center my-2">Theresia Lulu</p>
                <p className="flex items-center justify-center my-2">UI/UX Design</p>
              </div>
            </div>
            <div>
              <div className="flex items-center justify-center my-2">
                <Image
                  className="rounded-3xl"
                  src="/img/about/maya.png"
                  alt="profile"
                  width={133}
                  height={136}
                />
              </div>
              <div>
                <p className="flex items-center justify-center my-2">Himayatul Millah</p>
                <p className="flex items-center justify-center my-2">SM dan Backend</p>
              </div>
            </div>
            <div>
              <div className="flex items-center justify-center my-2">
                <Image
                  className="rounded-3xl"
                  src="/img/about/haekal.jpg"
                  alt="profile"
                  width={133}
                  height={136}
                />
              </div>
              <div>
                <p className="flex items-center justify-center my-2">Muhammad Haekal</p>
                <p className="flex items-center justify-center my-2">Backend</p>
              </div>
            </div>
            <div>
              <div className="flex items-center justify-center my-2">
                <Image
                  className="rounded-3xl"
                  src="/img/about/diko.png"
                  alt="profile"
                  width={133}
                  height={136}
                />
              </div>
              <div>
                <p className="flex items-center justify-center my-2">Andika Crishna Pramoedya</p>
                <p className="flex items-center justify-center my-2">Frontend</p>
              </div>
            </div>
            <div>
              <div className="flex items-center justify-center my-2">
                <Image
                  className="rounded-3xl"
                  src="/img/about/reza.jpg"
                  alt="profile"
                  width={133}
                  height={136}
                />
              </div>
              <div>
                <p className="flex items-center justify-center my-2">Maria Reza Desita</p>
                <p className="flex items-center justify-center my-2">Frontend</p>
              </div>
            </div>
            <div>
              <div className="flex items-center justify-center my-2">
                <Image
                  className="rounded-3xl"
                  src="/img/about/lala.jpeg"
                  alt="profile"
                  width={133}
                  height={136}
                />
              </div>
              <div>
                <p className="flex items-center justify-center my-2">Siti Urmilah Sasmita</p>
                <p className="flex items-center justify-center my-2">Frontend</p>
              </div>
            </div>
            <div>
              <div className="flex items-center justify-center my-2">
                <Image
                  className="rounded-3xl"
                  src="/img/about/intan.jpg"
                  alt="profile"
                  width={133}
                  height={136}
                />
              </div>
              <div>
                <p className="flex items-center justify-center my-2">Intan Khairunnisa F</p>
                <p className="flex items-center justify-center my-2">Android</p>
              </div>
            </div>
            <div>
              <div className="flex items-center justify-center my-2">
                <Image
                  className="rounded-3xl"
                  src="/img/about/ivo.jpg"
                  alt="profile"
                  width={133}
                  height={136}
                />
              </div>
              <div>
                <p className="flex items-center justify-center my-2">Ivo Herid Lesmana</p>
                <p className="flex items-center justify-center my-2">Android</p>
              </div>
            </div>
            <div>
              <div className="flex items-center justify-center my-2">
                <Image
                  className="rounded-3xl"
                  src="/img/about/yoga.jpg"
                  alt="profile"
                  width={133}
                  height={136}
                />
              </div>
              <div>
                <p className="flex items-center justify-center my-2">Yoga Dwi Prasetyo</p>
                <p className="flex items-center justify-center my-2">Android</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
