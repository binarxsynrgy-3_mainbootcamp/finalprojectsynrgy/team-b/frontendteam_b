import { useEffect } from 'react';
import { sessionUser } from '@/helpers/auth';
import { useAdminDispatch } from '@/redux/reducers/admin/slices';

export default function AdminDashboardContainer() {
  const token = sessionUser();
  const { admin, fetchDashboard } = useAdminDispatch();
  const { dashboard } = admin;

  useEffect(() => {
    fetchDashboard({ token });
  }, []);

  const RenderDashboardItem = ({ item, text, rp = false }) => (
    <div className="border rounded-lg flex flex-col items-center justify-center w-full px-5 py-5 gap-5 bg-white">
      <div>
        {rp && 'Rp. '}
        {item && (item).toLocaleString('id-ID')}
      </div>
      <div className="text-lg font-normal">{text}</div>
    </div>
  );

  const RenderDoubleDashboardItem = ({
    item1, item2, text1, text2,
  }) => (
    <div className="border rounded-lg flex flex-start items-center justify-center w-full px-5 py-5 bg-white">
      <div className="flex flex-col border-r items-center justify-center pr-8">
        {item1 && (item1).toLocaleString('id-ID')}
        <div className="text-lg font-normal">{text1}</div>
      </div>
      <div className="flex flex-col border-l items-center justify-center pl-8">
        {item2 && (item2).toLocaleString('id-ID')}
        <div className="text-lg font-normal">{text2}</div>
      </div>
    </div>
  );

  const renderDashboard = (item) => (
    <>
      {item && item.countCampaign ? (
        <div className="grid grid-cols-2  text-4xl font-semibold  gap-5">
          <RenderDashboardItem item={item.countCampaign} text="Jumlah Campaign Dibuat" />
          <RenderDashboardItem item={item.sumVerifiedDonation} text="Total Dana Terkumpul" rp />
          <RenderDoubleDashboardItem
            item1={item.countCampaignFinished}
            text1="Campaign Selesai"
            item2={item.countActiveCampaign}
            text2="CampaignBerjalan"
          />
          <RenderDashboardItem item={item.countUser} text="Total Pengguna" />
          <RenderDashboardItem item={item.countGallery} text="Galeri Karya Terpasang" />
        </div>
      )

        : <div className="flex justify-center items-center w-full"> loading </div>}
    </>

  );

  return (

    <div className="p-8 flex flex-col justify-center w-full">

      {renderDashboard(dashboard)}

    </div>

  );
}
