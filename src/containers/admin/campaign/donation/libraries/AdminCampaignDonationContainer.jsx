import Image from 'next/image';
import ReactPaginate from 'react-paginate';
import { useRouter } from 'next/router';
import EmptyImage from '@/components/EmptyImage';
import { convertDateToString } from '@/helpers/dateTime';
import { sessionUser } from '@/helpers/auth';
import { useAdminDispatch } from '@/redux/reducers/admin/slices';
import BuktiTransferModal from '@/components/PopupTransfer';
import { baseApiUrl } from '@/helpers/baseContents';

export default function AdminCampaignDonationContainer() {
  const router = useRouter();
  const token = sessionUser();
  const { campaignId } = router.query;
  const { admin, fetchDonations } = useAdminDispatch();
  const { donations, totalPages, page } = admin;

  const handlePageChange = async (selectedPage) => {
    await fetchDonations({ page: selectedPage.selected, campaignId, token });
  };

  const handleVerify = async (id, status) => {
    try {
      const endpoint = `${baseApiUrl}/admin/donation/verify/${id}?isVerified=${status}`;
      const response = await fetch(endpoint, {
        method: 'PUT',
        headers: {
          authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      console.log(data);
      window.location.reload();
    } catch (error) {
      console.log(error);
    }
  };

  const renderDonation = () => donations && donations.map((item) => (
    <>
      <div className="flex gap-3 p-5 rounded-lg text-xl border border-gray-150">
        <div className="p-3">
          {item.profileImage && item.profileFullName ? <Image src={item.profileImage} width="106" height="106" className="rounded-full" /> : <EmptyImage w={106} h={106} source="profile" />}
        </div>

        <div className="flex flex-start">

          <div className="flex flex-col gap-6 font-semibold" style={{ width: '680px' }}>
            <div>
              {item.profileFullName ? item.profileFullName : 'Anonim'}
            </div>
            <div className="font-normal">
              {item.notes}
            </div>

            <div className="flex justify-between w-3/4">
              <div className="font-normal">{convertDateToString(item.updatedAt)}</div>
              <div>
                Rp.
                {' '}
                {item.amount && (item.amount).toLocaleString('id-ID')}
                ,-
                {' '}
              </div>
            </div>
          </div>

          <div className="flex flex-col items-end justify-center">
            <div className="flex justify-end w-52">
              {item.status === 0 && (
              <div className="flex">
                <Image src="/img/donation/diproses.svg" width={25} height={25} />
                <div className="font-semibold ml-3 text-xl">Butuh Verifikasi</div>
              </div>
              )}
              {
                item.status === 1 && (
                  <div className="flex">
                    <Image src="/img/donation/berhasil.svg" width={25} height={25} />
                    <div className="font-semibold ml-3 text-xl">Terverifikasi</div>
                  </div>
                )
              }
              {
                item.status === 2 && (
                  <div className="flex">
                    <Image src="/img/donation/gagal.svg" width={25} height={25} />
                    <div className="font-semibold ml-3 text-xl">Gagal</div>
                  </div>
                )
              }
            </div>
            <div>
              <BuktiTransferModal
                item={item}
                onConfirm={() => handleVerify(item.id, true)}
                onReject={() => handleVerify(item.id, false)}
              />
            </div>
          </div>
        </div>
      </div>

    </>

  ));

  return (
    <div>
      {donations && donations.length > 0 ? renderDonation(donations)
        : (
          <div className="flex flex-col justify-center items-center p-4 gap-5">
            <EmptyImage source="campaign" h={320} w={320} />
            <div>Belum ada donasi yang dilakukan</div>
          </div>
        )}
      <div className={`${totalPages <= 1 && 'hidden'} flex justify-center`}>
        <ReactPaginate
          containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
          activeClassName="border border-green-250 px-2 rounded-md"
          initialPage={page}
          pageCount={totalPages}
          previousLabel="<"
          nextLabel=">"
          onPageChange={(e) => handlePageChange(e)}
        />
      </div>

    </div>
  );
}
