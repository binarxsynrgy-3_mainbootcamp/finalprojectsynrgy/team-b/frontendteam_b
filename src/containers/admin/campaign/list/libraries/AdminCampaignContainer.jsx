import { useEffect } from 'react';
import Image from 'next/image';
import ReactPaginate from 'react-paginate';
import Link from 'next/link';
import { sessionUser } from '@/helpers/auth';
import { useAdminDispatch } from '@/redux/reducers/admin/slices';
import EmptyImage from '@/components/EmptyImage';
import Breadcrumb from '@/components/Breadcrumb';

export default function AdminCampaignContainer() {
  const token = sessionUser();
  const { admin, fetchCampaigns } = useAdminDispatch();
  const { campaigns, page, totalPages } = admin;

  useEffect(() => {
    fetchCampaigns({ token });
  }, []);

  const pages = [
    { name: 'List Campaign Masuk', href: '#', current: true },
  ];
  const handlePageChange = async (selectedPage) => {
    await fetchCampaigns({ page: selectedPage.selected, token });
  };

  const renderCampaigns = () => campaigns && campaigns.map((item) => (
    <div key={item.id} className="grid grid-cols-6 border-t-2 pt-4 my-2 break-words text-xl">
      <div className="col-span-1">
        {item.imgUrl ? (<Image src={item.imgUrl} width="170" height="170" className="object-cover bg-gray-50 rounded-lg" />)
          : <EmptyImage w={170} h={170} source="campaign" />}
      </div>
      <div className="col-span-5 px-3">
        <div className="flex flex-col px-3 gap-2">

          <div className="grid grid-cols-8">
            <div className="col-span-1">
              Judul
            </div>
            <div className="col-span-7 font-semibold">
              {item.title}
            </div>
          </div>

          <div className="grid grid-cols-8">
            <div className=" col-span-1 ">
              Deskripsi
            </div>
            <div className="col-span-5 text-justify pr-8">
              {item.description && item.description.length > 200 ? `${item.description.substr(0, 200)}...` : item.description}
            </div>
            <div className="col-span-2 flex flex-col text-justify">
              {
                item.status === 0 && (
                  <div className="flex">
                    <Image src="/img/donation/diproses.svg" width={25} height={25} />
                    <div className="font-semibold ml-3">Perlu Verifikasi</div>
                  </div>
                )
              }
              {
                item.status === 1 && (
                  item.daysLeft < 0
                    ? (
                      <div className="flex">
                        <Image src="/img/donation/overdue.svg" width={25} height={25} />
                        <div className="font-semibold ml-3">Lewat Batas Waktu</div>
                      </div>
                    )
                    : (
                      <div className="flex">
                        <Image src="/img/donation/berhasil.svg" width={25} height={25} />
                        <div className="font-semibold ml-3">Terverifikasi</div>
                      </div>
                    )
                )
               }
              {
                item.status === 2 && (
                  <div className="flex">
                    <Image src="/img/donation/gagal.svg" width={25} height={25} />
                    <div className="font-semibold ml-3">Gagal</div>
                  </div>
                )
               }
              <Link href={`/admin/campaign/${item.id}/detail`}>
                <button type="button" className=" text-lg underline text-gray-650 hover:text-green-250 hover:underline cursor-pointer">Tinjau Campaign</button>
              </Link>
            </div>
          </div>

        </div>

      </div>

    </div>
  ));
  return (
    <div className="p-8 flex flex-col justify-center border rounded-xl mb-8 bg-white">
      <div className="flex flex-col gap-1 text-xl">
        <div className="-ml-4">
          <Breadcrumb pages={pages} />
        </div>
        <div>Kelola campaign pelaku industri kreatif</div>
      </div>
      {
        campaigns && campaigns.length > 0 ? renderCampaigns() : 'loading'
      }
      <div className={`${totalPages <= 1 && 'hidden'} flex justify-center`}>
        <ReactPaginate
          containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
          activeClassName="border border-green-250 px-2 rounded-md"
          initialPage={page}
          pageCount={totalPages}
          previousLabel="<"
          nextLabel=">"
          onPageChange={(e) => handlePageChange(e)}
        />
      </div>
    </div>
  );
}
