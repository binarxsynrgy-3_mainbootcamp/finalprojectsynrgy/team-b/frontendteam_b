import Image from 'next/image';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import EmptyImage from '@/components/EmptyImage';
import Reward from '@/components/Reward/libraries/Reward';

export default function AdminCampaignInfoContainer() {
  const { campaign } = useCampaignDispatch();
  const { campaignDetail, rewards } = campaign;

  const renderDetail = (item) => (
    <div className="flex flex-col text-gray-650 border-b-2 border-gray-150 gap-5">
      <div className="font-semibold text-5xl">{item.title}</div>
      <div className="flex justify-center my-10">
        {item.imgUrl ? <Image className="object-contain" src={item.imgUrl} width="1032" height="638" /> : <EmptyImage h={638} w={1032} source="campaign" />}
      </div>

      <div className="text-xl text-justify my-5">{item.description}</div>
    </div>
  );

  const renderRewards = () => rewards && rewards.map((item) => (
    <Reward hideBase {...item} key={`reward-${item.id}`} />
  ));

  return (
    <div>
      {campaignDetail && campaignDetail.id ? renderDetail(campaignDetail) : 'loading'}
      <div className="flex flex-start justify-center items-center gap-5 mt-10">
        {rewards && rewards.length > 0 ? renderRewards() : (
          <div className="flex flex-col justify-center items-center p-4 gap-5">
            <EmptyImage source="campaign" h={320} w={320} />
            <div>Tidak ada data reward</div>
          </div>
        )}
      </div>

    </div>
  );
}
