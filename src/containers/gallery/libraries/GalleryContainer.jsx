/* eslint-disable react/no-array-index-key */
import EmptyImage from '@/components/EmptyImage';

export default function GalleryContainer() {
  return (
    <div className="container mx-auto p-10">

      <div className="flex flex-col justify-center items-center p-4 gap-5">
        <EmptyImage source="campaign" h={320} w={320} />
        <div className="font-semibold">Halaman masih dalam pengembangan</div>
      </div>
    </div>
  );
}
