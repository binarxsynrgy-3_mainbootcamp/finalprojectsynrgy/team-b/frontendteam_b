module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
    'plugin:@next/next/recommended',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'react/react-in-jsx-scope': 'off',
    'react/prop-types': 'off',
    'import/prefer-default-export': 'off',
    'react/jsx-props-no-spreading': 'off',
    'import/no-unresolved': 0,
    'import/extensions': 0,
    'jsx-a11y/anchor-is-valid': 0,
    '@next/next/no-img-element': 0,
    '@next/next/link-passhref': 0,
    '@next/next/no-html-link-for-pages': 'off',
  },
};
